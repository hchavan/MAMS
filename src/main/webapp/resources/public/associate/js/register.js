var app = angular.module("register", ['ngMessages'])

app.directive('ngFiles', [ '$parse', function($parse) {

	function fn_link(scope, element, attrs) {
		var onChange = $parse(attrs.ngFiles);
		element.on('change', function(event) {
			onChange(scope, {
				$files : event.target.files
			});
		});
	}
	;

	return {
		link : fn_link
	}
} ]).controller('registerCtrl', [ '$scope', '$http', function($scope, $http) {

	var username = "";
	$scope.fileuploadV = false;
	$scope.checked = false;
	$scope.address = false;
	$scope.regg = true;
	$scope.loading = false;
	$scope.FS1 = false;
	$scope.STR1 = false;
	$scope.TIN1 = false;
	$scope.SE1 = false;
	$scope.LDC1 = false;
	$scope.MSME1 = false;
	$scope.invalidEmail = false;
	$scope.vm = {};
	$scope.vm.userRole = "ASSOCIATE";

	$scope.next = function() {
		
		if($scope.vm.telePhoneNo == null || $scope.vm.telePhoneNo.length < 10){
			return;
		}else if ($scope.vm.rPincode == null || $scope.vm.rPincode.length < 10) {
			return;
		}else if ($scope.vm.bPincode == null || $scope.vm.bPincode.length < 10) {
			return;
		}
		
		$scope.fileuploadV = false;
		$scope.regg = false;
		$scope.address = true;
	};

	$scope.back = function() {
		$scope.fileuploadV = false;
		$scope.regg = true;
		$scope.address = false;
	};

	$scope.register = function() {
		$scope.loading = true;
		$scope.checked = true;
		var postData = {
			companyName : $scope.vm.companyName,
			registeredNo : $scope.vm.registeredNo,
			tinNo : $scope.vm.tinNo,
			panNo : $scope.vm.panNo,
			cstNo : $scope.vm.cstNo,
			serviceTXregiNo : $scope.vm.serviceTXregiNo,
			telePhoneNo : $scope.vm.telePhoneNo,
			mobNo : $scope.vm.mobNo,
			rAddressLine1 : $scope.vm.rAddressLine1,
			rAddressLine2 : $scope.vm.rAddressLine2,
			rState : $scope.vm.rState,
			rTown : $scope.vm.rTown,
			rPincode : $scope.vm.rPincode,
			bAddressLine1 : $scope.vm.bAddressLine1,
			bAddressLine2 : $scope.vm.bAddressLine2,
			bState : $scope.vm.bState,
			bTown : $scope.vm.bTown,
			bPincode : $scope.vm.bPincode,
			webAddress : $scope.vm.webAddress,
			payeeName : $scope.vm.payeeName,
			bankName : $scope.vm.bankName,
			accountNo : $scope.vm.accountNo,
			ifsc_Code : $scope.vm.ifsc_Code,
			rout_swift_No : $scope.vm.rout_swift_No,
			associateName : $scope.vm.associateName,
			associateSPOC : $scope.vm.associateSPOC,
			associateEmail : $scope.vm.associateEmail,
			userRole : 'ASSOCIATE'
		}

		console.log(postData);
		$http({
			method : 'POST',
			url : '/associateUser',
			data : postData,
			headers : {
				"Content-Type" : "application/json",
				"Accept" : "text/plain"
			}
		}).then(function(response) {
			console.log(postData);
			
			if(response.statusText == "OK"){
				username = response.data;
				$scope.loading = false;
				$scope.fileuploadV = true;
				$scope.regg = false;
				$scope.address = false;
				$scope.checked = false;
			}else {
				alert("There is some Problem please try again...")
			}
			
		});
	};
	$scope.fileupload = function() {
		$scope.checked = true;
		var domain = document.domain;
		window.location.replace('http://'+domain+':8080/');
		alert("User Registered Successfully");
	};
	$scope.change = function(check) {
		if (check) {
			$scope.vm.bAddressLine1 = $scope.vm.rAddressLine1;
			$scope.vm.bAddressLine2 = $scope.vm.rAddressLine2;
			$scope.vm.bTown = $scope.vm.rTown;
			$scope.vm.bState = $scope.vm.rState;
			$scope.vm.bPincode = $scope.vm.rPincode;
		} else {
			$scope.vm.bAddressLine1 = "";
			$scope.vm.bAddressLine2 = "";
			$scope.vm.bTown = "";
			$scope.vm.bState = "";
			$scope.vm.bPincode = "";
		}
	};
	$scope.FS = function() {
		$scope.FS1 = true;
		console.log(file1.name);
		var formData = new FormData();
		formData.append("file", file1.files[0]);
		formData.append("doctyp_id", '1');
		formData.append("username", '' + username);
		$http({
			method : 'POST',
			url : '/uploadFiles',
			headers : {
				'Content-Type' : undefined
			},
			data : formData,
			transformRequest : function(data, headersGetterFunction) {
				return data;
			}
		}).success(function(data1, status) {
			$scope.FS1 = false;
		}).error(function(data, status) {
			$scope.FS1 = false;
			alert("file already uploaded");
		});
	};
	$scope.STR = function() {
		$scope.STR1 = true;
		console.log(file2.files[0]);
		var formData = new FormData();
		formData.append("file", file2.files[0]);
		formData.append("doctyp_id", '2');
		formData.append("username", '' + username);
		$http({
			method : 'POST',
			url : '/uploadFiles',
			headers : {
				'Content-Type' : undefined
			},
			data : formData,
			transformRequest : function(data, headersGetterFunction) {
				return data;
			}
		}).success(function(data1, status) {
			$scope.STR1 = false;
		}).error(function(data, status) {
			$scope.STR1 = false;
			alert("file already uploaded");
		});
	};
	$scope.TIN = function() {
		$scope.TIN1 = true;
		console.log(file3.files[0]);
		var formData = new FormData();
		formData.append("file", file3.files[0]);
		formData.append("doctyp_id", '3');
		formData.append("username", '' + username);
		$http({
			method : 'POST',
			url : '/uploadFiles',
			headers : {
				'Content-Type' : undefined
			},
			data : formData,
			transformRequest : function(data, headersGetterFunction) {
				return data;
			}
		}).success(function(data1, status) {
			$scope.TIN1 = false;
		}).error(function(data, status) {
			$scope.TIN1 = false;
			alert("file already uploaded");
		});
	};
	$scope.SE = function() {
		$scope.SE1 = true;
		console.log(file4.files[0]);
		var formData = new FormData();
		formData.append("file", file4.files[0]);
		formData.append("doctyp_id", '4');
		formData.append("username", '' + username);
		$http({
			method : 'POST',
			url : '/uploadFiles',
			headers : {
				'Content-Type' : undefined
			},
			data : formData,
			transformRequest : function(data, headersGetterFunction) {
				return data;
			}
		}).success(function(data1, status) {
			$scope.SE1 = false;
		}).error(function(data, status) {
			$scope.SE1 = false;
			alert("file already uploaded");
		});
	};
	$scope.LDC = function() {
		$scope.LDC1 = true;
		console.log(file5.files[0]);
		var formData = new FormData();
		formData.append("file", file5.files[0]);
		formData.append("doctyp_id", '5');
		formData.append("username", '' + username);
		$http({
			method : 'POST',
			url : '/uploadFiles',
			headers : {
				'Content-Type' : undefined
			},
			data : formData,
			transformRequest : function(data, headersGetterFunction) {
				return data;
			}
		}).success(function(data1, status) {
			$scope.LDC1 = false;
		}).error(function(data, status) {
			$scope.LDC1 = false;
			alert("file already uploaded");
		});
	};
	$scope.MSME = function() {
		$scope.MSME1 = true;
		console.log(file6.files[0]);
		var formData = new FormData();
		formData.append("file", file6.files[0]);
		formData.append("doctyp_id", '6');
		formData.append("username", '' + username);
		$http({
			method : 'POST',
			url : '/uploadFiles',
			headers : {
				'Content-Type' : undefined
			},
			data : formData,
			transformRequest : function(data, headersGetterFunction) {
				return data;
			}
		}).success(function(data1, status) {
			$scope.MSME1 = false;
		}).error(function(data, status) {
			$scope.MSME1 = false;
			alert("file already uploaded");
		});
	};

} ]);

app.directive('emailValidate', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
            	
            	console.log(element);
                console.log(attrs);
                
                scope.invalidEmail = false;
                scope.vm.email = viewValue;
                
            	var x = viewValue;
    			var atpos = x.indexOf("@");
    			var dotpos = x.lastIndexOf(".");
    			if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
    				scope.invalidEmail = true;
    			}else{
    				scope.invalidEmail = false;
    			}
            });
        }
    };
});
