var app = angular.module("register", []);

var scope;
app.controller('registerCtrl', [ '$scope', '$http', function($scope, $http) {

	$scope.am = {};
	scope = $scope;
	$scope.checked = false;
	$scope.loading = false;
	$scope.invalidMobile = false;
	$scope.invalidEmail = false;

	/*
	 * setInterval( function() { }, 1000);
	 */
	$scope.adminRegister = function() {
		$scope.invalidMobile = false;
		$scope.invalidEmail = false;

		var mobile = "" + $scope.am.mobNo;

		if (mobile.length < 10) {
			$scope.invalidMobile = true;
			return;
		} else {
			$scope.am.email = document.getElementById("email").value
			var x = "" + $scope.am.email;
			var atpos = x.indexOf("@");
			var dotpos = x.lastIndexOf(".");
			if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
				$scope.invalidEmail = true;
				return;
			}
		}

		$scope.checked = true;
		$scope.loading = true;

		var postData = {
			fullName : $scope.am.fullName,
			email : $scope.am.email,
			mobNo : $scope.am.mobNo,
			userRole : 'ADMIN'
		}

		$http({
			method : 'POST',
			url : '/adminUser',
			data : postData,
			headers : {
				"Content-Type" : "application/json",
				"Accept" : "text/plain"
			}
		}).success(function(data, status) {
			$scope.checked = false;
			$scope.loading = false;
			console.log(postData);
			alert(data);
			window.location.replace("../index.html");
		}).error(function(data, status) {
			$scope.checked = false;
			$scope.loading = false;
			console.log(postData);
			alert(data)
		});
	};

} ]);

app.directive('validate', function() {
	return {
		restrict : 'A',
		require : 'ngModel', // require: '^form',

		link : function(scope, element, attrs, ctrl) {
			console.log('======================');
			console.log(scope);
			console.log(element);
			console.log(attrs);
			console.log(ctrl);
			console.log(scope.am);

			/*
			 * var x = $scope.am.email; var atpos = x.indexOf("@"); var dotpos =
			 * x.lastIndexOf("."); if (atpos < 1 || dotpos < atpos + 2 || dotpos +
			 * 2 >= x.length) { $scope.invalidEmail = true; return; }
			 */
		}
	};
});

app.directive('positiveInteger', function() {
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, element, attrs, ctrl) {
			ctrl.$parsers.unshift(function(viewValue) {
				var INTEGER_REGEXP = /^\d+$/;
				if (INTEGER_REGEXP.test(viewValue)) { // it is valid
					ctrl.$setValidity('positiveInteger', true);
					return viewValue;
				} else { // it is invalid, return undefined (no model update)
					ctrl.$setValidity('positiveInteger', false);
					return undefined;
				}
			});
		}
	};
});
app.directive('emailValidate', function() {
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, element, attrs, ctrl) {
			ctrl.$parsers.unshift(function(viewValue) {

				console.log(element);
				console.log(attrs);

				scope.invalidEmail = false;
				scope.am.email = viewValue;

				var x = viewValue;
				var atpos = x.indexOf("@");
				var dotpos = x.lastIndexOf(".");
				if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
					scope.invalidEmail = true;
				} else {
					scope.invalidEmail = false;
				}
			});
		}
	};
});
