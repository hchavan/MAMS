var app = angular.module('rPassword', [ 'ngMessages' ]);

app.controller('resetPasswordCtrl', [ '$scope', '$http',
		function($scope, $http) {

			$scope.re = {}
			$scope.resetPassword = function() {
				
				if($scope.re.password != $scope.re.confirmPassword){
					return;
				}
				if($scope.re.password =="" || $scope.re.confirmPassword == ""){
					alert("Enter password")
					return;
				}
				
				var postData = "token="+localStorage.getItem("token")+"&newPassword="+$scope.re.password;

				$http(
						{
							method : 'POST',
							url : '/reset_password',
							data : postData,
							headers : {
								"Content-Type" : "application/x-www-form-urlencoded"
							}
						}).success(function(data, status) {
							alert(data);
							window.location.replace("index.html");
						}).error(function(data, status) {
							alert(data);
						});
				
			}
		} ]);

app.directive(
		'checkPasswordsMatch',
		function() {
			return {
				require : 'ngModel',
				link : function(scope, elm, attrs, ngModel) {
					ngModel.$validators.checkPasswordsMatch = function(
							modelValue, viewValue) {
						if (scope.re && scope.re.password && viewValue) {
							return scope.re.password === viewValue;
						}
						return true;
					};
				}
			};
		});