var app = angular.module('cPassword', []);

app.controller('changePasswordCtrl', [ '$scope', '$http',
		function($scope, $http) {

			var url = window.location.search;
			var tokenn = url.split('=')[1];
			var postData = "token="+tokenn;
			$scope.onLoad = false;
			$scope.loading = true;
			
			$scope.msg = "This password reset link is no longer valid.";

			$http({
				method : 'POST',
				url : '/user/checkTokan',
				data : postData,
				headers : {
					"Content-Type" : "application/x-www-form-urlencoded"
				}
			}).success(function(data, status) {
				$scope.loading = false;
				if(data == "ready to resetPassword"){
					window.location.replace("resetPassword.html");
					localStorage.setItem("token", tokenn);
				}else {
					$scope.msg = data;
					$scope.onLoad = true;
				}
				$scope.msg = data;
				//alert(data);
			}).error(function(data, status) {
				$scope.onLoad = true;
				$scope.loading = false;
				//alert(data);
			});

		} ]);