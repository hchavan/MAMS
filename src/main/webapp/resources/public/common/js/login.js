angular
		.module("login", [])
		.controller(
				'loginCtrl',
				[
						'$scope',
						'$http',
						function($scope, $http) {

							$scope.as = {};
							$scope.checked = false;
							$scope.minMaxLength = false;
							$scope.loading = false;
							$scope.upper = false;
							$scope.lower = false;
							$scope.number = false;
							$scope.special = false;

							$scope.login = function(password) {

								$scope.checked = true;
								$scope.loading = true;
								$scope.minMaxLength = false;
								$scope.upper = false;
								$scope.lower = false;
								$scope.number = false;
								$scope.special = false;

								var minMaxLength = /^[\s\S]{8,32}$/, upper = /[A-Z]/, lower = /[a-z]/, number = /[0-9]/;

								if (!minMaxLength.test(password)) {
									$scope.minMaxLength = true;
									$scope.checked = false;
									$scope.loading = false;
								} else if (!upper.test(password)) {
									$scope.upper = true;
									$scope.checked = false;
									$scope.loading = false;
								} else if (!lower.test(password)) {
									$scope.lower = true;
									$scope.loading = false;
									$scope.checked = false;
								} else if (!number.test(password)) {
									$scope.number = true;
									$scope.checked = false;
									$scope.loading = false;
								} else {
									var postData = 'username='
											+ $scope.as.username + '&password='
											+ $scope.as.password;

									$http(
											{
												method : 'POST',
												url : '/authenticate',
												data : postData,
												headers : {
													"Content-Type" : "application/x-www-form-urlencoded",
													"X-Login-Ajax-call" : 'true'
												}
											})
											.then(
													function(response) {
														$scope.checked = false;
														$scope.loading = false;
														console.log(response);
														var domain = document.domain;
														if (response.data.msg == "Login Successful") {

															if (response.data.userRole == "[ROLE_ASSOCIATE]") {
																window.location
																		.replace("../associate/associateHomePage.html");
															} else if (response.data.userRole == "[ROLE_ACCOUNT_MANAGER]") {
																window.location
																		.replace("../accountManager/accountManagerHomePage.html");
															} else if (response.data.userRole == "[ROLE_FINANCE_MANAGER]") {
																window.location
																		.replace("../financialManager/financeManagerHomePage.html");
															} else if (response.data.userRole == "[ROLE_DIRECTOR]") {
																window.location
																		.replace("../Director/directorHomePage.html");
															} else if (response.data.userRole == "[ROLE_ADMIN]") {
																window.location
																		.replace("../Admin/adminHomePage.html");
															} else if (response.data.userRole == "[ROLE_OP_MANAGER]") {
																window.location
																		.replace("../oprationManager/opManagerHomePage.html");
															} else if (response.data.userRole == "[ROLE_TENDER_TEAM]") {
																window.location
																		.replace("../tenderMaster/tenderTeamHomePage.html");
															}

														} else {
															alert("incorrect username and password ...");
															$scope.as.username = "";
															$scope.as.password = "";
														}

													});

								}
							}

							$scope.resetPassword = function() {
								var postData = "username=" + $scope.username;
								$http(
										{
											method : 'POST',
											url : '/forgot_password',
											data : postData,
											headers : {
												"Content-Type" : "application/x-www-form-urlencoded"
											}
										}).success(function(data, status) {
											alert(data);
											window.location.reload();
										}).error(function(data, status) {
											alert(data);
										});
							}

						} ]);