var app = angular.module('ttDetails',[]);

app.controller('tenderDetailsController',['$scope','$http',
						function($scope, $http) {

							$scope.tt = {};
							$scope.uploadfiles = {};
							var postData = "id="+localStorage.getItem("tenderId");
							$scope.tt.isMasconEligibleToBid = "Yes"

							/** *******Get All Uploaded List******** */
							$http(
									{
										method : 'POST',
										url : '/getTenderById',
										data:postData,
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										}
									})
									.success(
											function(data, status) {
												$scope.tt=data;
												$scope.tt.emd=parseInt(data.emd);
												$scope.tt.tenderFee=parseInt(data.tenderFee);
												$scope.tt.estimatedTenderValue=parseInt(data.estimatedTenderValue);
												$scope.tt.quantity=parseInt(data.quantity);
												$scope.tt.contactNo=parseInt(data.contactNo);
												$scope.tt.BG_DD_SC=parseInt(data.bg_dd_sc);
												
												if($scope.tt.isMasconEligibleToBid == "true"){
													$scope.tt.isMasconEligibleToBid = "Yes";
												}else {
													$scope.tt.isMasconEligibleToBid = "No";
												}
												
												console.log(data);
											}).error(function(data, status) {
										alert("error" + data)
									});

							$scope.availableCategorys = [
									"Select Category Type", "A", "B", "C" ];
							$scope.selectedavailableCategory = $scope.availableCategorys[0];

							$scope.createProject = function() {
								window.location.replace("createProject.html");
							}

						} ]);

jQuery(document).ready(function($) {

	if (window.history && window.history.pushState) {

		window.history.pushState('forward', null, null);

		$(window).on('popstate', function() {
			window.location.replace("opManagerHomePage.html");
		});

	}
});

