var app = angular.module('associateDetails',['angularUtils.directives.dirPagination']);

app.controller('associateDetailsCtrl',['$scope','$http',
						function($scope, $http) {

							$scope.as = {};
							$scope.uploadfiles = {};

							/** *******Get All Uploaded List******** */
							$http(
									{
										method : 'GET',
										url : '/getAssociateUser/'
												+ localStorage
														.getItem("associateUserId"),
										headers : {
											'Content-Type' : 'application/json'
										}
									})
									.success(
											function(data, status) {
												$scope.as = data;
												$scope.as.accountNo = parseInt(data.accountNo);
												$scope.as.bPincode = parseInt(data.bPincode);
												$scope.as.cstNo = parseInt(data.cstNo);
												$scope.as.mobNo = parseInt(data.mobNo);
												$scope.as.rPincode = parseInt(data.rPincode);
												$scope.as.registeredNo = parseInt(data.registeredNo);
												$scope.as.rout_swift_No = parseInt(data.rout_swift_No);
												$scope.as.serviceTXregiNo = parseInt(data.serviceTXregiNo);
												$scope.as.telePhoneNo = parseInt(data.telePhoneNo);
												$scope.as.tinNo = parseInt(data.tinNo);
												console.log($scope.as);

												$http(
														{
															method : 'POST',
															url : '/getFilesOfUser',
															data : 'username='
																	+ $scope.as.username,
															headers : {
																'Content-Type' : 'application/x-www-form-urlencoded'
															}
														})
														.success(
																function(data,
																		status) {
																	console
																			.log(data);
																	$scope.uploadfiles = data;
																})
														.error(
																function(data,
																		status) {
																	console
																			.log(data);
																});

											}).error(function(data, status) {
										alert("error" + data)
									});

							$scope.availableCategorys = [
									"Select Category Type", "A", "B", "C" ];
							$scope.selectedavailableCategory = $scope.availableCategorys[0];

							$scope.approve = function() {

								if ($scope.selectedavailableCategory != $scope.availableCategorys[0]) {

									var postData = "id=" + $scope.as.id
											+ "&category="
											+ $scope.selectedavailableCategory;

									$http(
											{
												method : 'POST',
												url : '/approveAssociateUserByAM',
												data : postData,
												headers : {
													'Content-Type' : 'application/x-www-form-urlencoded'
												}
											})
											.success(
													function(data, status) {
														alert("success" + data);
														window.location
																.replace("accountManagerHomePage.html");
													}).error(
													function(data, status) {
														alert("error" + data);
													});

								} else {
									alert("select category for associate user");
								}

							}

						} ]);

app.filter('setDecimal', function($filter) {
	return function(input, places) {
		if (isNaN(input))
			return input;
		// If we want 1 decimal place, we want to mult/div by 10
		// If we want 2 decimal places, we want to mult/div by 100, etc
		// So use the following to create that factor
		var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
		return Math.round(input * factor) / factor;
	};
});

jQuery(document).ready(function($) {

	if (window.history && window.history.pushState) {

		window.history.pushState('forward', null, null);

		$(window).on('popstate', function() {
			window.location.replace("accountManagerHomePage.html");
		});

	}
});
