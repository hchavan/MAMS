var app = angular.module('tdTeam', ['ui.bootstrap','ngMessages']);

app
		.controller(
				'homepageCtrl',
				[
						'$scope',
						'$http',
						function($scope, $http) {
							
							$scope.datepickerOptions = {
							        format: 'dd-mm-yyyy',
							        language: 'en',
							        startDate: "01-01-1900",
							        endDate: "31-12-2200",
							        autoclose: true,
							        weekStart: 0
							    }

							console.log("homepageCtrl");

							$scope.tt = {};
							//$scope.isMasconEligibleToBidArry = [ 'Yes', 'No' ];
							//$scope.tt.isMasconEligibleToBid = $scope.isMasconEligibleToBidArry[0];
							$scope.tt.isMasconEligibleToBid = "Yes"
							$scope.submit = function() {
								$scope.tt.preBidMeetingDate = document
										.getElementById("preBidMeetingDate").value;
								$scope.tt.preBidMeetingTime = document
										.getElementById("preBidMeetingTime").value;
								$scope.tt.tenderSubmissionEndDate = document
										.getElementById("tenderSubmissionEndDate").value;
								$scope.tt.tenderSubmissionEndTime = document
										.getElementById("tenderSubmissionEndTime").value;
								$scope.tt.tenderOpeningDate = document
										.getElementById("tenderOpeningDate").value;
								$scope.tt.tenderOpeningTime = document
										.getElementById("tenderOpeningTime").value;
								console.log($scope.tt);

								if ($scope.tt.isMasconEligibleToBid == "Yes") {
									$scope.tt.isMasconEligibleToBid = "true";
								} else {
									$scope.tt.isMasconEligibleToBid = "false";
								}
								var postData = {

									tenderId 				: $scope.tt.tenderId,
									tenderReferenceNo 		: $scope.tt.tenderReferenceNo,
									organisationChain 		: $scope.tt.organisationChain,
									locationDistrict 		: $scope.tt.locationDistrict,
									state 					: $scope.tt.state,
									projectDesccription 	: $scope.tt.projectDesccription,
									preBidMeetingDate 		: $scope.tt.preBidMeetingDate,
									preBidMeetingTime 		: $scope.tt.preBidMeetingTime,
									tenderSubmissionEndDate : $scope.tt.tenderSubmissionEndDate,
									tenderSubmissionEndTime : $scope.tt.tenderSubmissionEndTime,
									tenderOpeningDate 		: $scope.tt.tenderOpeningDate,
									tenderOpeningTime 		: $scope.tt.tenderOpeningTime,
									emd 					: $scope.tt.emd,
									tenderFee 				: $scope.tt.tenderFee,
									estimatedTenderValue 	: $scope.tt.estimatedTenderValue,
									bg_dd_sc 				: $scope.tt.BG_DD_SC,
									quantity 				: $scope.tt.quantity,
									projectDuration 		: $scope.tt.projectDuration,
									webSite 				: $scope.tt.webSite,
									scopOfWork 				: $scope.tt.scopOfWork,
									contactPersonName 		: $scope.tt.contactPersonName,
									email 					: $scope.tt.email,
									contactNo 				: $scope.tt.contactNo,
									requirmentFromClient 	: $scope.tt.requirmentFromClient,
									requirmentFromMascon 	: $scope.tt.requirmentFromMascon,
									isMasconEligibleToBid 	: $scope.tt.isMasconEligibleToBid

								}

								$http({
									method : 'POST',
									url : '/storeTender',
									data : postData,
									headers : {
										"Content-Type" : "application/json",
										"Accept" : "text/plain"
									}
								}).success(function(data, status) {
									window.location.reload();
									alert(data);
								}).error(function(data, status) {
									alert(data);
								});

							}

						} ]);
