package com.mscs.app.controller;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mscs.app.dto.NewUserDTO;
import com.mscs.app.dto.TenderDTO;
import com.mscs.app.dto.TenderData;
import com.mscs.app.helpers.MailSendService;
import com.mscs.app.helpers.RandomPasswordGenerator;
import com.mscs.app.model.TendarMaster;
import com.mscs.app.services.TenderService;
import com.mscs.app.services.UserService;

/**
 * This controller is for tender team related rests
 * 
 * Date : May 6, 2016
 * 
 * @author Himmat
 */
@Controller
public class TenderTeamController
{

     private static final Logger LOGGER	= Logger.getLogger(TenderTeamController.class);
     @Autowired
     UserService		 userService;
     @Autowired
     TenderService		 tenderService;
     @Autowired
     private JavaMailSender	 mailSender;
     @Autowired
     private MailSendService	 mailSendService;
     @Autowired
     private Environment	 env;

     /**
      * rest for register tender team user.
      * 
      * @param user
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/tenderTeamUser", method = RequestMethod.POST)
     public void createUser(@RequestBody NewUserDTO user, final HttpServletRequest request, HttpServletResponse response) throws Exception {
	  System.out.println("tender team user contrl called successfully.");

	  // Generate random username and password
	  String username = user.getEmail().trim();
	  String password = RandomPasswordGenerator.getRandomPassword().trim();
	  String encodedPassword = new BCryptPasswordEncoder().encode(password);
	  System.out.println("tenderTeamUser username===== " + username);
	  System.out.println("tenderTeamUser password===== " + password);

	  userService.createTenderTeamUser(user.getrAddressLine1(), user.getrAddressLine2(), user.getrState(), user.getrTown(), user.getrPincode(), username, encodedPassword,
	            user.getUserRole(), user.getMobNo(), true, user.getFullName(), user.getEmail());

	  // send back response string
	  PrintWriter out = response.getWriter();
	  out.print("user registered successfully");
	  out.flush();
	  // generaate html message for registration
	  String htmlMessage = createHtmlPageforRegistration(user, username, password, user.getFullName());
	  // send the email with html message
	  final String subject = "Registration Successful";
	  // send mail throw another thread
	  mailSendService.sendMail(user.getEmail(), htmlMessage, subject);

     }

     /**
      * Rest for store tender
      * 
      * @param tender
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/storeTender", method = RequestMethod.POST)
     public void storeTender(@RequestBody TenderDTO tender, final HttpServletRequest request, HttpServletResponse response) throws Exception {

	  System.out.println("storetender rest called" + tender);

	  tenderService.storeTender(tender);

	  // send back response string
	  PrintWriter out = response.getWriter();
	  out.print("user registered successfully");
	  out.flush();

     }

     /**
      * Rest for getting list of tenders
      * 
      * @param request
      * @param response
      * @return
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/getTenderList", method = RequestMethod.GET)
     public @ResponseBody List<TenderData> getTenderList(HttpServletRequest request, HttpServletResponse response) {
	  LOGGER.info("getTenderList rest called");

	  List<TenderData> tenderList = tenderService.getTenderList();

	  return tenderList;
     }

     /**
      * Rest for getting tender by id.
      * 
      * @param id
      * @param request
      * @param response
      * @return
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/getTenderById", method = RequestMethod.POST)
     public @ResponseBody TendarMaster getTenderById(@RequestParam(value = "id", required = true) String id, HttpServletRequest request, HttpServletResponse response) {
	  LOGGER.info("getTenderById rest called");

	  TendarMaster tender = tenderService.getTenderById(Long.parseLong(id));

	  return tender;
     }

     /**
      * for generate html message.
      * 
      * @param user
      * @param username
      * @param password
      * @param associateName
      * @return
      */
     private String createHtmlPageforRegistration(final NewUserDTO user, final String username, final String password, final String associateName) {
	  String message = "<!DOCTYPE html>" + "<html>" + "<head>" + "</head>" + "<body  style=\"background-color: #f5f5f5;height: 300px;\">" +

	  "<div style=\"background-color:#2D3E50;height: 50px\" >" + "<div style=\"color: orange;text-align: left;margin-top: 25px;margin-left: 10px\">" + "<h1>MAMS<h1>" + "</div>"
	            + "</div>" +

	  "<div >" + "<div style=\"margin-left: 6px\">" + " Hello <b>" + associateName + "</b>,<br><br></div>"
	            + "<p style=\"text-indent: 5em;\">Your registration has been successfully done.<br>"
	            + "<p style=\"text-indent: 5em;\">Your username is your registered email.<br>" + "<p style=\"text-indent: 5em;\">Your password is as below :<br><br><br>"
	            + "<p style=\"text-indent: 5em;\">password=" + password + "<br><br>" +

	  "<div style=\"background-color:#2D3E50\">" + "<div style=\"color: white;text-align: center;padding:5px;margin-top: 55px;\">" + "Copyright � Aibsinc.com" + "</div>"
	            + "</div>" + "</body>" + "</html>";
	  System.out.println(" HTML message...." + message);

	  return message;
     }

     /**
      * Return the exception to the client
      * 
      * @param exc
      * @return
      */
     @ExceptionHandler(Exception.class)
     public ResponseEntity<String> errorHandler(Exception exc) {
	  LOGGER.info(exc.getMessage(), exc);
	  return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
     }
}
