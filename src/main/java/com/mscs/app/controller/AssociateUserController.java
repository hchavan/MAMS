
package com.mscs.app.controller;

import java.io.PrintWriter;
import java.util.Calendar;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mscs.app.dto.NewUserDTO;
import com.mscs.app.helpers.MailSendService;
import com.mscs.app.helpers.RandomPasswordGenerator;
import com.mscs.app.helpers.RandomUsernameGenerator;
import com.mscs.app.helpers.StaticConfige;
import com.mscs.app.model.PasswordResetToken;
import com.mscs.app.model.User;
import com.mscs.app.services.FileUploadService;
import com.mscs.app.services.UserService;

/**
 * Rest Controller for associate User
 * 
 * Date : May 3, 2016
 * 
 * @author Himmat
 */
@Controller
@PropertySource("classpath:email.properties")
@ComponentScan({ "com.mscs.app.helpers" })
public class AssociateUserController
{

     private static final Logger LOGGER	= Logger.getLogger(AssociateUserController.class);

     @Autowired
     UserService		 userService;
     @Autowired
     private JavaMailSender	 mailSender;
     @Autowired
     private MailSendService	 mailSendService;
     @Autowired
     private Environment	 env;
     @Autowired
     FileUploadService		 fileUplopadService;

     /**
      * Method for register associate user.
      * 
      * @param user
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/associateUser", method = RequestMethod.POST)
     public void createUser(@RequestBody NewUserDTO user, final HttpServletRequest request, HttpServletResponse response) throws Exception {
	  System.out.println("user contrl called successfully.\n" + user);

	  PrintWriter out = response.getWriter();
	  // Generate random username and password
	  String username = RandomUsernameGenerator.getRandomUserName().trim();
	  String password = RandomPasswordGenerator.getRandomPassword().trim();
	  String encodedPassword = new BCryptPasswordEncoder().encode(password);
	  System.out.println("associate user username===== " + username);
	  System.out.println("associate user password===== " + password);

	  userService.createUser(user.getCompanyName(), user.getRegisteredNo(), user.getTinNo(), user.getPanNo(), user.getCstNo(),
	            user.getServiceTXregiNo(), user.getTelePhoneNo(), user.getrAddressLine1(), user.getrAddressLine2(), user.getrState(),
	            user.getrTown(), user.getrPincode(), user.getbAddressLine1(), user.getbAddressLine2(), user.getbState(), user.getbTown(),
	            user.getbPincode(), username, encodedPassword, user.getWebAddress(), user.getPayeeName(), user.getUserRole(), user.getBankName(),
	            user.getAccountNo(), user.getIfsc_Code(), user.getRout_swift_No(), user.getAssociateName(), user.getAssociateSPOC(),
	            user.getMobNo(), user.getAssociateEmail(), user.isActive(), user.isApproveByAM(), user.isApproveByFM(), user.isApproveByD());

	  // generaate html message for registration
	  String htmlMessage = createHtmlPageforRegistration(user, username, password, user.getAssociateName());
	  // send the email with html message
	  final String subject = "Registration Successful";
	  // send mail throw another thread
	  mailSendService.sendMail(user.getAssociateEmail(), htmlMessage, subject);
	  // send back username
	  out.print(username);
	  out.flush();

     }

     /**
      * Rest for sending password change link to registered email.
      * 
      * @param username
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/forgot_password", method = RequestMethod.POST)
     public void forgotPassword(@RequestParam("username") String username, final HttpServletRequest request, final HttpServletResponse response)
               throws Exception {
	  System.out.println("forgot_passord called successfully.");
	  PrintWriter out = response.getWriter();

	  final String appUrl = StaticConfige.httpProtocol + "://" + request.getServerName() + ":" + request.getServerPort()
	            + request.getContextPath();
	  // generate the token
	  final String token = UUID.randomUUID().toString();
	  // get the user by username
	  User db_user = userService.findUserByUsername(username);

	  if (db_user == null) {
	       out.print("That username doesn't match any user accounts. Are you sure you've registered?");
	       out.flush();
	  }

	  // generaate html message for registration
	  String htmlMessage = createHtmlPageforForgotPassword(db_user, token, appUrl);

	  userService.forgotPassword(db_user, token);

	  // send the email with html message
	  final String subject = "Reset password";
	  //get email id.
	  String emailId="";
	  if(db_user.getUserRole().equals("ASSOCIATE"))
	       emailId=db_user.getAssociateEmail();
	  else
	       emailId=db_user.getEmail();
	  
	  // send mail throw another thread
	  mailSendService.sendMail(emailId, htmlMessage, subject);

	  out.print("To reset your password, follow the instructions in the email we've just sent you");
	  out.flush();

     }

     /**
      * Rest for checking the tokan is valid or not.
      * 
      * @param token
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/user/checkTokan", method = RequestMethod.POST)
     public void checkTokan(@RequestParam("token") final String token, final HttpServletRequest request, final HttpServletResponse response)
               throws Exception {

	  System.out.println(" in checkTokan controller.");
	  System.out.println("Token is===  " + token);

	  final PasswordResetToken passwordToken = userService.getPasswordToken(token);

	  // final User user = passwordToken.getUser();
	  final Calendar cal = Calendar.getInstance();
	  PrintWriter out = response.getWriter();
	  if (passwordToken == null) {
	       final String message = "invalid token.";
	       out.print(message);
	       out.flush();

	  } else if (passwordToken.isPasswordChanged() == true) {

	       final String message = "Sorry!This password reset link is no longer valid.Becuase Password changed one time using this link";
	       out.print(message);
	       out.flush();

	  } else if ((passwordToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {

	       final String message = "Sorry!This password reset link is no longer valid.Becuase Tokan is expired due to time limitation.";
	       out.print(message);
	       out.flush();
	  } else {

	       final String message = "ready to resetPassword";
	       out.print(message);
	       out.flush();
            
	  }

     }

     /**
      * Rest for change the password and sending the password changed email.
      * 
      * @param token
      * @param newPassword
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/reset_password", method = RequestMethod.POST)
     public void resetPassword(@RequestParam("token") String token, @RequestParam("newPassword") String newPassword, final HttpServletRequest request,
               final HttpServletResponse response) throws Exception {
	  System.out.println("reset_password called successfully.");

	  // final User user = passwordToken.getUser();
	  final Calendar cal = Calendar.getInstance();
	  final PasswordResetToken passwordToken = userService.getPasswordToken(token);
	  PrintWriter out = response.getWriter();
	  if (passwordToken == null) {
	       final String message = "invalid token.";

	       out.print(message);
	       out.flush();

	  } else if (passwordToken.isPasswordChanged() == true) {

	       final String message = "Sorry!This token is no longer valid.Becuase Password changed one time using this token";
	       out.print(message);
	       out.flush();

	  } else if ((passwordToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {

	       final String message = "Sorry!This password reset link is no longer valid.Becuase Tokan is expired due to time limitation.";
	       out.print(message);
	       out.flush();

	  } else {

	       final User db_user = passwordToken.getUser();

	       // reset password and merge user
	       userService.savePasswordResetUser(db_user, newPassword,passwordToken);

	       String newMessage = "You have recently changed the password for user<b>" + " " + db_user.getFullName() + "</b>";
	       // generaate html message for registration
	       String htmlMessage = createGenericEmail(db_user, newMessage);

	       // send the email with html message
	       final String subject = " Password changed";
	       // send mail throw another thread
	       mailSendService.sendMail(db_user.getEmail(), htmlMessage, subject);

	       out.print("Password changed");
	       out.flush();
	  }

     }

     /**
      * This method is used for construct html message for sending mail
      * 
      * @param user
      * @param username
      * @param password
      * @param associateName
      * @return
      */
     private String createHtmlPageforRegistration(final NewUserDTO user, final String username, final String password, final String associateName) {
	  String message = "<!DOCTYPE html>" + "<html>" + "<head>" + "</head>" + "<body  style=\"background-color: #f5f5f5;height: 300px;\">" +

	  "<div style=\"background-color:#2D3E50;height: 50px\" >"
	            + "<div style=\"color: orange;text-align: left;margin-top: 25px;margin-left: 10px\">" + "<h1>MAMS<h1>" + "</div>" + "</div>" +

	  "<div >" + "<div style=\"margin-left: 6px\">" + " Hello <b>" + associateName + "</b>,<br><br></div>"
	            + "<p style=\"text-indent: 5em;\">Your registration has been successfully done.<br>"
	            + "<p style=\"text-indent: 5em;\">Your username and password is as below :<br><br><br>"
	            + "<p style=\"text-indent: 5em;\">username=" + username + "<br><br>" + "<p style=\"text-indent: 5em;\">password=" + password
	            + "<br><br>" +

	  "<div style=\"background-color:#2D3E50\">" + "<div style=\"color: white;text-align: center;padding:5px;margin-top: 55px;\">"
	            + "Copyright � Aibsinc.com" + "</div>" + "</div>" + "</body>" + "</html>";
	  System.out.println(" HTML message...." + message);

	  return message;
     }

     /**
      * This method for create html message for forget password.
      * 
      * @param db_user
      * @param token
      * @param appUrl
      * @return
      */
     private String createHtmlPageforForgotPassword(User db_user, String token, String appUrl) {
	  // TODO Auto-generated method stub

	  String message = "<!DOCTYPE html>" + "<html>" + "<head>" + "</head>" + "<body  style=\"background-color: #f5f5f5;height: 300px;\">" +

	  "<div style=\"background-color:#2D3E50;height: 50px\" >"
	            + "<div style=\"color: orange;text-align: left;margin-top: 25px;margin-left: 10px\">" + "<h1>Aibs Inc<h1>" + "</div>" + "</div>" +

	  "<div >" + "<div style=\"margin-left: 6px\">" + " Hello " + "User" + "<br><br></div>"
	            + "<p style=\"text-indent: 5em;\">If you want to change the password for userName<b>" + " " + db_user.getFullName() + "</b><br>"
	            + "<p style=\"text-indent: 5em;\">Then you have go thow below link.<br><br><br>" + "<p style=\"text-indent: 5em;\"><a href=\""
	            + appUrl + "/resources/public/changePassword.html?token=" + token + "\">Click here to change password</a></p>" + "</div>" +

	  "<div style=\"background-color:#2D3E50\">" + "<div style=\"color: white;text-align: center;padding:5px;margin-top: 55px;\">"
	            + "Copyright � Aibsinc.com" + "</div>" + "</div>" + "</body>" + "</html>";
	  System.out.println(" HTML message...." + message);

	  return message;
     }

     private String createGenericEmail(final User user, final String newmessage) {
	  String message = "<!DOCTYPE html>" + "<html>" + "<head>" + "</head>" + "<body  style=\"background-color: #f5f5f5;height: 200px;\">" +

	  "<div style=\"background-color:#2D3E50;height: 50px\" >"
	            + "<div style=\"color: orange;text-align: left;margin-top: 25px;margin-left: 10px\">" + "<h1>Aibs Inc<h1>" + "</div>" + "</div>" +

	  "<div >" + "<div style=\"margin-left: 6px\">" + " Hello <b>" + user.getFullName() + "</b>,<br><br></div>"
	            + "<p style=\"text-indent: 5em;\"> " + newmessage + " .<br>" + "</div>" +

	  "<div style=\"background-color:#2D3E50\">" + "<div style=\"color: white;text-align: center;padding:5px;margin-top: 55px;\">"
	            + "Copyright � Aibsinc.com" + "</div>" + "</div>" + "</body>" + "</html>";
	  System.out.println(" HTML message...." + message);

	  return message;
     }

     /**
      * Return the exception to the client
      * 
      * @param exc
      * @return
      */
     @ExceptionHandler(Exception.class)
     public ResponseEntity<String> errorHandler(Exception exc) {
	  LOGGER.info(exc.getMessage(), exc);
	  return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
     }
}
