package com.mscs.app.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.HandlerMapping;

import com.mscs.app.model.DocTypes;
import com.mscs.app.model.UploadedFile;
import com.mscs.app.model.User;
import com.mscs.app.services.FileUploadService;
import com.mscs.app.services.UserService;

/**
 * Rest controller for related file uploading services
 * 
 * Date : May 3, 2016
 * 
 * @author Himmat
 */
@Controller
public class FileController1
{
     private static final Logger LOGGER	= Logger.getLogger(FileController1.class);

     @Autowired
     UserService		 userService;
     @Autowired
     FileUploadService		 fileUplopadService;

     /**
      * This Rest is used to upload file to server
      * 
      * @param doctyp_id
      * @param username
      * @param request
      * @param response
      * @throws Exception
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/uploadFiles", method = RequestMethod.POST)
     public void upload(@RequestParam(value = "doctyp_id", required = true) String doctyp_id,
               @RequestParam(value = "username", required = true) String username, MultipartHttpServletRequest request, HttpServletResponse response)
                         throws Exception {

	  LOGGER.info("/uploadFiles/ Called.");
	  // MultipartHttpServletRequest request=(MultipartHttpServletRequest)
	  // request1;

	  // extract Description from request
	  // String doctyp_id = request.getParameter("doctyp_id");
	  // String username=request.getParameter("username");

	  System.out.println(" =============DocType_ID===== " + doctyp_id);
	  System.out.println(" =============userName===== " + username);
	  DocTypes docTypes = fileUplopadService.findDocTypeById(Long.parseLong(doctyp_id));
	  User user = userService.findUserByUsername(username);

	  System.out.println(" ===========DOCTYPE*************===== " + docTypes);
	  System.out.println(" =============USER*************===== " + user);
	  String associateName = user.getAssociateName();

	  String rawUploadPath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
	  LOGGER.info("Raw Upload Path:" + rawUploadPath);
	  System.out.println("request in upload second round" + rawUploadPath);

	  String uploadFolderPath = getUploadFolderPath(rawUploadPath.split("/"));
	  System.out.println("uploadFolderPath " + uploadFolderPath);

	  /*
	   * UserDetails userDetails = (UserDetails)
	   * SecurityContextHolder.getContext().getAuthentication().getPrincipal
	   * () ; String userName = userDetails.getUsername().toString();
	   */

	  // Getting uploaded files from the request object
	  Map<String, MultipartFile> fileMap = request.getFileMap();

	  // Maintain a list to send back the files info. to the client side
	  List<UploadedFile> uploadedFiles = new ArrayList<UploadedFile>();

	  // Iterate through the map
	  for (MultipartFile multipartFile : fileMap.values()) {

	       UploadedFile fileInfo = getUploadedFileInfo(multipartFile, uploadFolderPath, username, docTypes, associateName);

	       // Save the file info to database
	       saveFileToDatabase(fileInfo);

	       // Save the file to local disk
	       saveFileToLocalDisk(multipartFile, uploadFolderPath, associateName, username);

	       // adding the file info to the list
	       uploadedFiles.add(fileInfo);

	  }

	  response.getWriter().println("file uploaded successfully");

	  // return "The file uploaded successfully.";

     }

     /**
      * ----Rest for getting list of files uploaded by user
      * 
      * @param username
      * @param request
      * @param response
      * @return
      */
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = "/getFilesOfUser", method = RequestMethod.POST)
     public @ResponseBody List<UploadedFile> getFileOfUser(@RequestParam(value = "username", required = true) String username,
               final HttpServletRequest request, final HttpServletResponse response) {

	  List<UploadedFile> fileList = fileUplopadService.getFilesOfUser(username);

	  return fileList;

     }

     /**
      * This rest for download file with get method
      * 
      * @param response
      * @param fileId
      */
     @RequestMapping(value = "/get/{fileId}", method = RequestMethod.GET)
     public void getFile(HttpServletResponse response, @PathVariable Long fileId) {

	  UploadedFile dataFile = fileUplopadService.downloadRowDocFile(fileId);

	  File file = new File(dataFile.getPath(), dataFile.getName());

	  try {
	       response.setContentType(dataFile.getType());
	       response.setHeader("Content-disposition", "attachment; filename=\"" + dataFile.getName() + "\"");

	       FileCopyUtils.copy(FileUtils.readFileToByteArray(file), response.getOutputStream());

	  } catch (IOException e) {
	       e.printStackTrace();
	  }
     }

     private String getDestinationLocation(String associateName, String userName) {

	  final String directoryPath = "C:\\myMAMS\\" + associateName + "\\" + userName; // for
	                                                                                 // UNIX
	                                                                                 // use
	                                                                                 // /
	  File dir = new File(directoryPath);
	  if (dir.exists() && dir.isDirectory()) {
	       return directoryPath;
	  } else if (dir.mkdirs()) {
	       LOGGER.info("Directory " + directoryPath + " is created!");
	  } else {
	       LOGGER.info("Failed to create directory " + directoryPath);
	       return null;
	  }
	  return dir.getPath();
     }

     private String getUploadFolderPath(String[] str) {

	  String newStr = "";
	  for (int i = 7; i < str.length; i++) {
	       newStr += str[i] + "\\"; // for UNIX use /
	  }

	  return newStr;
     }

     private void saveFileToLocalDisk(MultipartFile multipartFile, String folderPath, String associateName, String userName)
               throws IOException, FileNotFoundException {

	  String outputFileName = getOutputFilename(multipartFile, folderPath, associateName, userName);
	  System.out.println("outputFileName " + outputFileName);
	  FileCopyUtils.copy(multipartFile.getBytes(), new FileOutputStream(outputFileName));
     }

     private UploadedFile getUploadedFileInfo(MultipartFile multipartFile, String uploadFolderPath, String userName, DocTypes docTypes,
               String associateName) throws IOException {

	  DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	  Calendar calobj = Calendar.getInstance();
	  UploadedFile fileInfo = new UploadedFile();

	  // check if user is login
	  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  // if (!(auth instanceof AnonymousAuthenticationToken)) {

	  fileInfo.setName(multipartFile.getOriginalFilename());
	  fileInfo.setType(multipartFile.getContentType());
	  fileInfo.setSize(multipartFile.getSize());
	  fileInfo.setCreationDate(df.format(calobj.getTime()));
	  fileInfo.setModificationDate(df.format(calobj.getTime()));
	  fileInfo.setOwner(userName);
	  fileInfo.setPath(getStoreLocation(uploadFolderPath, associateName, userName));
	  fileInfo.setDocType(docTypes);

	  // }

	  return fileInfo;
     }

     private void saveFileToDatabase(UploadedFile uploadedFile) {

	  fileUplopadService.createFile(uploadedFile.getName(), uploadedFile.getType(), uploadedFile.getSize(), uploadedFile.getCreationDate(),
	            uploadedFile.getModificationDate(), uploadedFile.getOwner(), uploadedFile.getPath(), uploadedFile.getDocType());

     }

     private String getOutputFilename(MultipartFile multipartFile, String folderPath, String associateName, String userName) {

	  return getDestinationLocation(associateName, userName) + "\\" + folderPath + multipartFile.getOriginalFilename();
     }

     private String getStoreLocation(String uploadFolderPath, String instituteName, String userName) {

	  String s = getDestinationLocation(instituteName, userName) + "\\" + uploadFolderPath;
	  s = s.substring(0, s.length() - 1);
	  return s;
     }

     /**
      * Return the exception to the client
      * 
      * @param exc
      * @return
      */
     @ExceptionHandler(Exception.class)
     public ResponseEntity<String> errorHandler(Exception exc) {
	  LOGGER.info(exc.getMessage(), exc);
	  return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
     }

}
