package com.mscs.app.services;

import java.util.List;

import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mscs.app.dao.FileUploadRepository;
import com.mscs.app.dao.UserRepository;
import com.mscs.app.model.DocTypes;
import com.mscs.app.model.UploadedFile;

/**
 *
 * Business service for FileUpload entity related operations
 *
 */

@Service
public class FileUploadService
{

     private static final Logger  LOGGER = Logger.getLogger(FileUploadService.class);

     @Autowired
     private FileUploadRepository fileRepository;

     @Autowired
     private UserRepository	  userRepository;

     /**
      *
      * creates a new file in the database
      * 
      * @param name
      *             - the name of the file
      * @param type
      *             - the type of the file
      * @param size
      *             - the size of the file
      * @param creationDate
      *             - the creation date of the file
      * @param modificationDate
      *             - the modification date of the file
      * @param owner
      *             - the owner of the file
      * @param path
      *             - the path of the file
      * @param description
      *             - the description of the file
      *
      */
     @Transactional
     public void createFile(String name, String type, Long size, String creationDate, String modificationDate,
               String owner, String path, DocTypes docTypes) {

	  if (!fileRepository.isFileNameSizePathAvailable(name, size, path)) {
	       throw new IllegalArgumentException("The file already exists.");
	  }
	  UploadedFile file = new UploadedFile(name, type, size, creationDate, modificationDate, owner, path, docTypes);

	  try {
	       fileRepository.save(file);
	       System.out.println("The file uploaded successfully.");
	  } catch (Exception e) {
	       e.printStackTrace();
	  }
     }

     @Transactional(readOnly = true)
     public List<UploadedFile> findUsersFiles(String userName) {
	  return fileRepository.findUsersFiles(userName);
     }


     @Transactional
     public int deleteFile(Long id) {
	  return fileRepository.deleteFile(id);
     }

     @Transactional
     public List<UploadedFile> findFilesByInstName(String instituteName) {
	  return fileRepository.findFilesByInstName(instituteName);
     }

     @Transactional(readOnly = true)
     public List<UploadedFile> findAllFiles() {
	  return fileRepository.findAllFiles();
     }

     public List<UploadedFile> getFilesOfUser(String userName) {
	  return fileRepository.getFilesOfUser(userName);
     }

     @Transactional(readOnly = true)
     public UploadedFile downloadRowDocFile(Long id) {

	  return fileRepository.downloadRowDocFile(id);
     }
     @Transactional
     public DocTypes findDocTypeById(long parseInt) {
	  // TODO Auto-generated method stub
	  return fileRepository.getDocTypeById(parseInt);
     }

}
