package com.mscs.app.services;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mscs.app.dao.TenderRepository;
import com.mscs.app.dto.TenderDTO;
import com.mscs.app.dto.TenderData;
import com.mscs.app.model.TendarMaster;

/**
 * Service for tendor related rests
 * 
 * Date : May 9, 2016
 * 
 * @author Himmat
 */
@Service
public class TenderService
{
     private static final Logger LOGGER	= Logger.getLogger(TenderService.class);

     @Autowired
     TenderRepository		 tendorRepository;

     @Transactional
     public void storeTender(TenderDTO tendor) {
	  // TODO Auto-generated method stub

	  TendarMaster targetTendor = new TendarMaster();

	  // copy properties of TendorDTO to TendarMaster
	  BeanUtils.copyProperties(tendor, targetTendor);

	  if (tendor.getIsMasconEligibleToBid().equalsIgnoreCase("true"))
	       targetTendor.setMasconEligibleToBid(true);

	  tendorRepository.save(targetTendor);
     }

     @Transactional
     public List<TenderData> getTenderList() {
	  // TODO Auto-generated method stub
	  return tendorRepository.getTenderList();
     }

     @Transactional
     public TendarMaster getTenderById(long id) {
	  // TODO Auto-generated method stub
	  return tendorRepository.getTenderById(id);
     }

}
