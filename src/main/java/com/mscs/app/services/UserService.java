package com.mscs.app.services;

import java.util.List;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.mscs.app.dao.PasswordTokenRepository;
import com.mscs.app.dao.TenderRepository;
import com.mscs.app.dao.UserRepository;
import com.mscs.app.dto.UserData;
import com.mscs.app.model.PasswordResetToken;
import com.mscs.app.model.User;
import com.mscs.app.model.tenderRelated.TenderProject;

/**
 *
 * Business service for User entity related operations
 *
 */
@Service
public class UserService
{

     private static final Logger  LOGGER	 = Logger.getLogger(UserService.class);

     private static final Pattern PASSWORD_REGEX = Pattern.compile("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}");

     private static final Pattern EMAIL_REGEX	 = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

     @Autowired
     private UserRepository	  userRepository;

     @Autowired
     PasswordTokenRepository	  passwordTokenRepository;

     @Autowired
     TenderRepository		  tenderRepository;

     /**
      * creates a new Associate user in the database
      */
     @Transactional
     public void createUser(String companyName, String registeredNo, String tinNo, String panNo, String cstNo, String serviceTXregiNo, String telePhoneNo, String getrAddressLine1,
                            String getrAddressLine2, String getrState, String getrTown, String getrPincode, String getbAddressLine1, String getbAddressLine2, String getbState,
                            String getbTown, String getbPincode, String username, String password, String webAddress, String payeeName, String userRole, String bankName,
                            String accountNo, String ifsc_Code, String rout_swift_No, String associateName, String associateSPOC, String mobNo, String associateEmail,
                            boolean active, boolean approveByAM, boolean approveByFM, boolean approveByD) {
	  // TODO Auto-generated method stub
	  if (!userRepository.isUsernameAvailable(username)) {
	       throw new IllegalArgumentException("The username is not available.");
	  }

	  User user = new User(companyName, registeredNo, tinNo, panNo, cstNo, serviceTXregiNo, telePhoneNo, getrAddressLine1, getrAddressLine2, getrState, getrTown, getrPincode,
	            getbAddressLine1, getbAddressLine2, getbState, getbTown, getbPincode, username, password, webAddress, payeeName, userRole, bankName, accountNo, ifsc_Code,
	            rout_swift_No, associateName, associateSPOC, mobNo, associateEmail, active, approveByAM, approveByFM, approveByD, null);

	  userRepository.save(user);
     }

     /**
      * creates a Admin user in the database
      */
     @Transactional
     public void createAdminUser(String getrAddressLine1, String getrAddressLine2, String getrState, String getrTown, String getrPincode, String username, String password,
                                 String userRole, String mobNo, boolean b, String fullName, String email) {
	  // TODO Auto-generated method stub
	  if (!userRepository.isUsernameAvailable(username)) {
	       throw new IllegalArgumentException("This username is already exits.");
	  }

	  User user = new User(getrAddressLine1, getrAddressLine2, getrState, getrTown, getrPincode, username, password, userRole, mobNo, b, fullName, email);

	  userRepository.save(user);
     }

     /**
      * creates a new Account manager user in the database
      */
     @Transactional
     public void createAcountManagerUser(String getrAddressLine1, String getrAddressLine2, String getrState, String getrTown, String getrPincode, String username,
                                         String encodedPassword, String userRole, String mobNo, boolean b, String fullName, String email) {
	  // TODO Auto-generated method stub
	  if (!userRepository.isUsernameAvailable(username)) {
	       throw new IllegalArgumentException("This email is already exits.Enter another email.because this will be your username..");
	  }

	  User user = new User(getrAddressLine1, getrAddressLine2, getrState, getrTown, getrPincode, username, encodedPassword, userRole, mobNo, b, fullName, email);

	  userRepository.save(user);

     }

     /**
      * creates a new Finance manager user in the database
      */
     @Transactional
     public void createFinanceManagerUser(String getrAddressLine1, String getrAddressLine2, String getrState, String getrTown, String getrPincode, String username,
                                          String encodedPassword, String userRole, String mobNo, boolean b, String fullName, String email) {
	  // TODO Auto-generated method stub
	  if (!userRepository.isUsernameAvailable(username)) {
	       throw new IllegalArgumentException("This email is already exits.Enter another email.because this will be your username..");
	  }

	  User user = new User(getrAddressLine1, getrAddressLine2, getrState, getrTown, getrPincode, username, encodedPassword, userRole, mobNo, b, fullName, email);

	  userRepository.save(user);

     }

     /**
      * creates a new Director user in the database
      */
     @Transactional
     public void createDirectorUser(String getrAddressLine1, String getrAddressLine2, String getrState, String getrTown, String getrPincode, String username,
                                    String encodedPassword, String userRole, String mobNo, boolean b, String fullName, String email) {
	  // TODO Auto-generated method stub
	  if (!userRepository.isUsernameAvailable(username)) {
	       throw new IllegalArgumentException("This email is already exits.Enter another email.because this will be your username..");
	  }

	  User user = new User(getrAddressLine1, getrAddressLine2, getrState, getrTown, getrPincode, username, encodedPassword, userRole, mobNo, b, fullName, email);

	  userRepository.save(user);

     }

     /**
      * creates a new tendrTeam user in the database
      */
     @Transactional
     public void createTenderTeamUser(String getrAddressLine1, String getrAddressLine2, String getrState, String getrTown, String getrPincode, String username,
                                      String encodedPassword, String userRole, String mobNo, boolean b, String fullName, String email) {
	  // TODO Auto-generated method stub
	  if (!userRepository.isUsernameAvailable(username)) {
	       throw new IllegalArgumentException("This email is already exits.Enter another email.because this will be your username..");
	  }

	  User user = new User(getrAddressLine1, getrAddressLine2, getrState, getrTown, getrPincode, username, encodedPassword, userRole, mobNo, b, fullName, email);

	  userRepository.save(user);

     }

     /**
      * creates a new Operational manager in the database
      */
     @Transactional
     public void createOpManagerUser(String getrAddressLine1, String getrAddressLine2, String getrState, String getrTown, String getrPincode, String username,
                                     String encodedPassword, String userRole, String mobNo, boolean b, String fullName, String email) {
	  // TODO Auto-generated method stub
	  if (!userRepository.isUsernameAvailable(username)) {
	       throw new IllegalArgumentException("This email is already exits.Enter another email.because this will be your username..");
	  }

	  User user = new User(getrAddressLine1, getrAddressLine2, getrState, getrTown, getrPincode, username, encodedPassword, userRole, mobNo, b, fullName, email);

	  userRepository.save(user);

     }

     public boolean checkUserisAvailableOrNot(String username) {
	  return userRepository.checkUserisAvailableOrNot(username);

     }

     @Transactional
     public User findUserByUsername(String username) {
	  return userRepository.findUserByUsername(username);
     }

     @Transactional
     public List<UserData> getAssociateListForAM() {
	  // TODO Auto-generated method stub
	  return userRepository.getAssociateListForAM();
     }

     @Transactional
     public User getAssociateUSer(long associateUserId) {
	  // TODO Auto-generated method stub
	  return userRepository.getAssociateUser(associateUserId);
     }

     @Transactional
     public void approveAssociateUserByAM(long user_id, String category) {
	  // TODO Auto-generated method stub

	  User user = userRepository.getAssociateUser(user_id);

	  if (user != null) {
	       user.setCategory(category);
	       user.setApproveByAM(true);
	  }

	  userRepository.mergeUser(user);

     }

     @Transactional
     public List<UserData> getAssociateListForFM() {
	  // TODO Auto-generated method stub
	  return userRepository.getAssociateListForFM();
     }

     @Transactional
     public List<UserData> getAssociateListForD() {
	  // TODO Auto-generated method stub
	  return userRepository.getAssociateListForD();
     }

     @Transactional
     public List<UserData> getAssociateListForOP() {
	  // TODO Auto-generated method stub
	  return userRepository.getAssociateListForOP();
     }

     @Transactional
     public void approveAssociateUserByFM(long user_id, String category) {
	  // TODO Auto-generated method stub
	  User user = userRepository.getAssociateUser(user_id);

	  if (user != null) {
	       user.setCategory(category);
	       user.setApproveByFM(true);
	  }

	  userRepository.mergeUser(user);

     }

     @Transactional
     public void approveAssociateUserByD(long user_id, String category) {
	  // TODO Auto-generated method stub
	  User user = userRepository.getAssociateUser(user_id);

	  if (user != null) {
	       user.setCategory(category);
	       user.setApproveByD(true);
	  }

	  userRepository.mergeUser(user);
     }

     @Transactional
     public void forgotPassword(User db_user, String token) {
	  // TODO Auto-generated method stub

	  User user = userRepository.findUserByUsername(db_user.getUsername());
	  final PasswordResetToken passwordResetToken = new PasswordResetToken(token, user);

	  passwordTokenRepository.save(passwordResetToken);

     }

     @Transactional
     public PasswordResetToken getPasswordToken(String passwordToken) {
	  // TODO Auto-generated method stub
	  return passwordTokenRepository.findByToken(passwordToken);
     }

     @Transactional
     public void savePasswordResetUser(final User db_user, final String newPassword, final PasswordResetToken paResetToken) {

	  // set new password
	  db_user.setPasswordDigest(new BCryptPasswordEncoder().encode(newPassword));
	  // set password changed with this token true.
	  paResetToken.setPasswordChanged(true);

	  passwordTokenRepository.mergeToekn(paResetToken);
	  userRepository.mergeUser(db_user);
     }

     @Transactional
     public void createTenderProject(TenderProject project) {
	  // TODO Auto-generated method stub
	  tenderRepository.saveTenderProject(project);
     }

}