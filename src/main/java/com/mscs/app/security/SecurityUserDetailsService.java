package com.mscs.app.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mscs.app.dao.UserRepository;
import com.mscs.app.model.User;

/**
 *
 * UserDetails service that reads the user credentials from the database, using
 * a JPA repository.
 *
 */
@Service
@ComponentScan({ "com.mscs.app.security" })

public class SecurityUserDetailsService implements UserDetailsService
{

     private static final Logger LOGGER	= Logger.getLogger(SecurityUserDetailsService.class);

     @Autowired
     private UserRepository	 userRepository;

     @Override

     public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

	  User user = userRepository.findUserByUsername(username);

	  System.out.println("============in service========" + user);

	  if (user == null) {
	       String message = "Username not found" + username;
	       LOGGER.info(message);
	       throw new UsernameNotFoundException(message);
	  }

	  List<GrantedAuthority> authorities = new ArrayList<>();
	  authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getUserRole()));

	  return new org.springframework.security.core.userdetails.User(username, user.getPasswordDigest(),
	            authorities);
     }
}
