package com.mscs.app.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class CustomFailureHandler implements AuthenticationFailureHandler
{

     @SuppressWarnings("deprecation")
     @Override
     public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
               AuthenticationException exception) throws IOException, ServletException {
	  // TODO Auto-generated method stub

	  if ("true".equals(request.getHeader("X-Login-Ajax-call"))) {

	       System.out.println(
	                 "====================login Failed================  " + exception.getLocalizedMessage());
	       
	      
	       response.getWriter().print("login failed");
	       response.getWriter().flush();

	  }

     }

}
