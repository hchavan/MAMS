package com.mscs.app.dto;


public class NewUserDTO
{

     private long    id;
     private String  companyName;
     private String  registeredNo;
     private String  tinNo;
     private String  panNo;
     private String  cstNo;
     private String  serviceTXregiNo;
     private String  telePhoneNo;
     private String  rAddressLine1;
     private String  rAddressLine2;
     private String  rState;
     private String  rTown;
     private String  rPincode;
     private String  bAddressLine1;
     private String  bAddressLine2;
     private String  bState;
     private String  bTown;
     private String  bPincode;
     private String  username;
     private String  passwordDigest;
     private String  webAddress;
     private String  payeeName;
     private String  userRole;
     private String  bankName;
     private String  accountNo;
     private String  ifsc_Code;
     private String  rout_swift_No;
     private String  associateName;
     private String  associateSPOC;
     private String  mobNo;
     private String  associateEmail;
     private boolean isActive	      = false;
     private boolean isApproveByAM    = false;
     private boolean isApproveByFM    = false;
     private boolean isApproveByD     = false;
     private String  category;
     private boolean isFirstTimeLogin = true;
     private String  fullName;
     private String  email;

     public NewUserDTO() {

     }

     public NewUserDTO( String companyName , String registeredNo , String tinNo , String panNo , String cstNo ,
               String serviceTXregiNo , String telePhoneNo , String rAddressLine1 , String rAddressLine2 ,
               String rState , String rTown , String rPincode , String bAddressLine1 , String bAddressLine2 ,
               String bState , String bTown , String bPincode , String username , String passwordDigest ,
               String webAddress , String payeeName , String userRole , String bankName , String accountNo ,
               String ifsc_Code , String rout_swift_No , String associateName , String associateSPOC , String mobNo ,
               String associateEmail , boolean isActive , boolean isApproveByAM , boolean isApproveByFM ,
               boolean isApproveByD , String category ) {
	  super();
	  this.companyName = companyName;
	  this.registeredNo = registeredNo;
	  this.tinNo = tinNo;
	  this.panNo = panNo;
	  this.cstNo = cstNo;
	  this.serviceTXregiNo = serviceTXregiNo;
	  this.telePhoneNo = telePhoneNo;
	  this.rAddressLine1 = rAddressLine1;
	  this.rAddressLine2 = rAddressLine2;
	  this.rState = rState;
	  this.rTown = rTown;
	  this.rPincode = rPincode;
	  this.bAddressLine1 = bAddressLine1;
	  this.bAddressLine2 = bAddressLine2;
	  this.bState = bState;
	  this.bTown = bTown;
	  this.bPincode = bPincode;
	  this.username = username;
	  this.passwordDigest = passwordDigest;
	  this.webAddress = webAddress;
	  this.payeeName = payeeName;
	  this.userRole = userRole;
	  this.bankName = bankName;
	  this.accountNo = accountNo;
	  this.ifsc_Code = ifsc_Code;
	  this.rout_swift_No = rout_swift_No;
	  this.associateName = associateName;
	  this.associateSPOC = associateSPOC;
	  this.mobNo = mobNo;
	  this.associateEmail = associateEmail;
	  this.isActive = isActive;
	  this.isApproveByAM = isApproveByAM;
	  this.isApproveByFM = isApproveByFM;
	  this.isApproveByD = isApproveByD;
	  this.category = category;
     }
     // constructor for another users like Admin,FM,AM,Direrctor

     public NewUserDTO( String rAddressLine1 , String rAddressLine2 , String rState , String rTown , String rPincode ,
               String username , String passwordDigest , String userRole , String mobNo , boolean isActive ,
               String fullName , String email ) {
	  super();
	  this.rAddressLine1 = rAddressLine1;
	  this.rAddressLine2 = rAddressLine2;
	  this.rState = rState;
	  this.rTown = rTown;
	  this.rPincode = rPincode;
	  this.username = username;
	  this.passwordDigest = passwordDigest;
	  this.userRole = userRole;
	  this.mobNo = mobNo;
	  this.isActive = isActive;
	  this.fullName = fullName;
	  this.email = email;
     }

     public long getId() {
	  return id;
     }

     public void setId(long id) {
	  this.id = id;
     }

     public String getCompanyName() {
	  return companyName;
     }

     public void setCompanyName(String companyName) {
	  this.companyName = companyName;
     }

     public String getRegisteredNo() {
	  return registeredNo;
     }

     public void setRegisteredNo(String registeredNo) {
	  this.registeredNo = registeredNo;
     }

     public String getTinNo() {
	  return tinNo;
     }

     public void setTinNo(String tinNo) {
	  this.tinNo = tinNo;
     }

     public String getPanNo() {
	  return panNo;
     }

     public void setPanNo(String panNo) {
	  this.panNo = panNo;
     }

     public String getCstNo() {
	  return cstNo;
     }

     public void setCstNo(String cstNo) {
	  this.cstNo = cstNo;
     }

     public String getServiceTXregiNo() {
	  return serviceTXregiNo;
     }

     public void setServiceTXregiNo(String serviceTXregiNo) {
	  this.serviceTXregiNo = serviceTXregiNo;
     }

     public String getTelePhoneNo() {
	  return telePhoneNo;
     }

     public void setTelePhoneNo(String telePhoneNo) {
	  this.telePhoneNo = telePhoneNo;
     }

     public String getrAddressLine1() {
	  return rAddressLine1;
     }

     public void setrAddressLine1(String rAddressLine1) {
	  this.rAddressLine1 = rAddressLine1;
     }

     public String getrAddressLine2() {
	  return rAddressLine2;
     }

     public void setrAddressLine2(String rAddressLine2) {
	  this.rAddressLine2 = rAddressLine2;
     }

     public String getrState() {
	  return rState;
     }

     public void setrState(String rState) {
	  this.rState = rState;
     }

     public String getrTown() {
	  return rTown;
     }

     public void setrTown(String rTown) {
	  this.rTown = rTown;
     }

     public String getrPincode() {
	  return rPincode;
     }

     public void setrPincode(String rPincode) {
	  this.rPincode = rPincode;
     }

     public String getbAddressLine1() {
	  return bAddressLine1;
     }

     public void setbAddressLine1(String bAddressLine1) {
	  this.bAddressLine1 = bAddressLine1;
     }

     public String getbAddressLine2() {
	  return bAddressLine2;
     }

     public void setbAddressLine2(String bAddressLine2) {
	  this.bAddressLine2 = bAddressLine2;
     }

     public String getbState() {
	  return bState;
     }

     public void setbState(String bState) {
	  this.bState = bState;
     }

     public String getbTown() {
	  return bTown;
     }

     public void setbTown(String bTown) {
	  this.bTown = bTown;
     }

     public String getbPincode() {
	  return bPincode;
     }

     public void setbPincode(String bPincode) {
	  this.bPincode = bPincode;
     }

     public String getUsername() {
	  return username;
     }

     public void setUsername(String username) {
	  this.username = username;
     }

     public String getPasswordDigest() {
	  return passwordDigest;
     }

     public void setPasswordDigest(String passwordDigest) {
	  this.passwordDigest = passwordDigest;
     }

     public String getWebAddress() {
	  return webAddress;
     }

     public void setWebAddress(String webAddress) {
	  this.webAddress = webAddress;
     }

     public String getPayeeName() {
	  return payeeName;
     }

     public void setPayeeName(String payeeName) {
	  this.payeeName = payeeName;
     }

     public String getUserRole() {
	  return userRole;
     }

     public void setUserRole(String userRole) {
	  this.userRole = userRole;
     }

     public String getBankName() {
	  return bankName;
     }

     public void setBankName(String bankName) {
	  this.bankName = bankName;
     }

     public String getAccountNo() {
	  return accountNo;
     }

     public void setAccountNo(String accountNo) {
	  this.accountNo = accountNo;
     }

     public String getIfsc_Code() {
	  return ifsc_Code;
     }

     public void setIfsc_Code(String ifsc_Code) {
	  this.ifsc_Code = ifsc_Code;
     }

     public String getRout_swift_No() {
	  return rout_swift_No;
     }

     public void setRout_swift_No(String rout_swift_No) {
	  this.rout_swift_No = rout_swift_No;
     }

     public String getAssociateName() {
	  return associateName;
     }

     public void setAssociateName(String associateName) {
	  this.associateName = associateName;
     }

     public String getAssociateSPOC() {
	  return associateSPOC;
     }

     public void setAssociateSPOC(String associateSPOC) {
	  this.associateSPOC = associateSPOC;
     }

     public String getMobNo() {
	  return mobNo;
     }

     public void setMobNo(String mobNo) {
	  this.mobNo = mobNo;
     }

     public String getAssociateEmail() {
	  return associateEmail;
     }

     public void setAssociateEmail(String associateEmail) {
	  this.associateEmail = associateEmail;
     }

     public boolean isActive() {
	  return isActive;
     }

     public void setActive(boolean isActive) {
	  this.isActive = isActive;
     }

     public boolean isApproveByAM() {
	  return isApproveByAM;
     }

     public void setApproveByAM(boolean isApproveByAM) {
	  this.isApproveByAM = isApproveByAM;
     }

     public boolean isApproveByFM() {
	  return isApproveByFM;
     }

     public void setApproveByFM(boolean isApproveByFM) {
	  this.isApproveByFM = isApproveByFM;
     }

     public boolean isApproveByD() {
	  return isApproveByD;
     }

     public void setApproveByD(boolean isApproveByD) {
	  this.isApproveByD = isApproveByD;
     }

     public String getCategory() {
	  return category;
     }

     public void setCategory(String category) {
	  this.category = category;
     }

     public boolean isFirstTimeLogin() {
	  return isFirstTimeLogin;
     }

     public void setFirstTimeLogin(boolean isFirstTimeLogin) {
	  this.isFirstTimeLogin = isFirstTimeLogin;
     }

     public String getFullName() {
	  return fullName;
     }

     public void setFullName(String fullName) {
	  this.fullName = fullName;
     }

     public String getEmail() {
	  return email;
     }

     public void setEmail(String email) {
	  this.email = email;
     }

     @Override
     public String toString() {
	  return "NewUserDTO [companyName=" + companyName + ", registeredNo=" + registeredNo + ", tinNo=" + tinNo
	            + ", panNo=" + panNo + ", cstNo=" + cstNo + ", serviceTXregiNo=" + serviceTXregiNo
	            + ", telePhoneNo=" + telePhoneNo + ", rAddressLine1=" + rAddressLine1 + ", rAddressLine2="
	            + rAddressLine2 + ", rState=" + rState + ", rTown=" + rTown + ", rPincode=" + rPincode
	            + ", bAddressLine1=" + bAddressLine1 + ", bAddressLine2=" + bAddressLine2 + ", bState=" + bState
	            + ", bTown=" + bTown + ", bPincode=" + bPincode + ", username=" + username + ", passwordDigest="
	            + passwordDigest + ", webAddress=" + webAddress + ", payeeName=" + payeeName + ", userRole="
	            + userRole + ", bankName=" + bankName + ", accountNo=" + accountNo + ", ifsc_Code=" + ifsc_Code
	            + ", rout_swift_No=" + rout_swift_No + ", associateName=" + associateName + ", associateSPOC="
	            + associateSPOC + ", mobNo=" + mobNo + ", associateEmail=" + associateEmail + ", isActive="
	            + isActive + ", isApproveByAM=" + isApproveByAM + ", isApproveByFM=" + isApproveByFM
	            + ", isApproveByD=" + isApproveByD + ", category=" + category + ", isFirstTimeLogin="
	            + isFirstTimeLogin + ", fullName=" + fullName + ", email=" + email + "]";
     }

}
