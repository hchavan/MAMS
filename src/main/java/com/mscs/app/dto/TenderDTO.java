package com.mscs.app.dto;

public class TenderDTO
{
     String  tenderId;
     String  tenderReferenceNo;
     String  organisationChain;
     String  locationDistrict;
     String  state;
     String  projectDesccription;
     String  preBidMeetingDate;
     String  preBidMeetingTime;
     String  tenderSubmissionEndDate;
     String  tenderSubmissionEndTime;
     String  tenderOpeningDate;
     String  tenderOpeningTime;
     String  emd;
     String  tenderFee;
     String  estimatedTenderValue;
     String  bg_dd_sc;
     String  quantity;
     String  projectDuration;
     String  webSite;
     String  scopOfWork;
     String  contactPersonName;
     String  email;
     String  contactNo;
     String  requirmentFromClient;
     String  requirmentFromMascon;
     String isMasconEligibleToBid;

     
     
     public TenderDTO() {
	
	
     }



     public TenderDTO( String tenderId , String tenderReferenceNo , String organisationChain , String locationDistrict , String state , String projectDesccription ,
                       String preBidMeetingDate , String preBidMeetingTime , String tenderSubmissionEndDate , String tenderSubmissionEndTime , String tenderOpeningDate ,
                       String tenderOpeningTime , String emd , String tenderFee , String estimatedTenderValue , String bg_dd_sc , String quantity , String projectDuration ,
                       String webSite , String scopOfWork , String contactPersonName , String email , String contactNo , String requirmentFromClient , String requirmentFromMascon ,
                       String isMasconEligibleToBid ) {
	  super();
	  this.tenderId = tenderId;
	  this.tenderReferenceNo = tenderReferenceNo;
	  this.organisationChain = organisationChain;
	  this.locationDistrict = locationDistrict;
	  this.state = state;
	  this.projectDesccription = projectDesccription;
	  this.preBidMeetingDate = preBidMeetingDate;
	  this.preBidMeetingTime = preBidMeetingTime;
	  this.tenderSubmissionEndDate = tenderSubmissionEndDate;
	  this.tenderSubmissionEndTime = tenderSubmissionEndTime;
	  this.tenderOpeningDate = tenderOpeningDate;
	  this.tenderOpeningTime = tenderOpeningTime;
	  this.emd = emd;
	  this.tenderFee = tenderFee;
	  this.estimatedTenderValue = estimatedTenderValue;
	  this.bg_dd_sc = bg_dd_sc;
	  this.quantity = quantity;
	  this.projectDuration = projectDuration;
	  this.webSite = webSite;
	  this.scopOfWork = scopOfWork;
	  this.contactPersonName = contactPersonName;
	  this.email = email;
	  this.contactNo = contactNo;
	  this.requirmentFromClient = requirmentFromClient;
	  this.requirmentFromMascon = requirmentFromMascon;
	  this.isMasconEligibleToBid = isMasconEligibleToBid;
     }



     /**
      * @return the tenderId
      */
     public String getTenderId() {
          return tenderId;
     }



     /**
      * @param tenderId the tenderId to set
      */
     public void setTenderId(String tenderId) {
          this.tenderId = tenderId;
     }



     /**
      * @return the tenderReferenceNo
      */
     public String getTenderReferenceNo() {
          return tenderReferenceNo;
     }



     /**
      * @param tenderReferenceNo the tenderReferenceNo to set
      */
     public void setTenderReferenceNo(String tenderReferenceNo) {
          this.tenderReferenceNo = tenderReferenceNo;
     }



     /**
      * @return the organisationChain
      */
     public String getOrganisationChain() {
          return organisationChain;
     }



     /**
      * @param organisationChain the organisationChain to set
      */
     public void setOrganisationChain(String organisationChain) {
          this.organisationChain = organisationChain;
     }



     /**
      * @return the locationDistrict
      */
     public String getLocationDistrict() {
          return locationDistrict;
     }



     /**
      * @param locationDistrict the locationDistrict to set
      */
     public void setLocationDistrict(String locationDistrict) {
          this.locationDistrict = locationDistrict;
     }



     /**
      * @return the state
      */
     public String getState() {
          return state;
     }



     /**
      * @param state the state to set
      */
     public void setState(String state) {
          this.state = state;
     }



     /**
      * @return the projectDesccription
      */
     public String getProjectDesccription() {
          return projectDesccription;
     }



     /**
      * @param projectDesccription the projectDesccription to set
      */
     public void setProjectDesccription(String projectDesccription) {
          this.projectDesccription = projectDesccription;
     }



     /**
      * @return the preBidMeetingDate
      */
     public String getPreBidMeetingDate() {
          return preBidMeetingDate;
     }



     /**
      * @param preBidMeetingDate the preBidMeetingDate to set
      */
     public void setPreBidMeetingDate(String preBidMeetingDate) {
          this.preBidMeetingDate = preBidMeetingDate;
     }



     /**
      * @return the preBidMeetingTime
      */
     public String getPreBidMeetingTime() {
          return preBidMeetingTime;
     }



     /**
      * @param preBidMeetingTime the preBidMeetingTime to set
      */
     public void setPreBidMeetingTime(String preBidMeetingTime) {
          this.preBidMeetingTime = preBidMeetingTime;
     }



     /**
      * @return the tenderSubmissionEndDate
      */
     public String getTenderSubmissionEndDate() {
          return tenderSubmissionEndDate;
     }



     /**
      * @param tenderSubmissionEndDate the tenderSubmissionEndDate to set
      */
     public void setTenderSubmissionEndDate(String tenderSubmissionEndDate) {
          this.tenderSubmissionEndDate = tenderSubmissionEndDate;
     }



     /**
      * @return the tenderSubmissionEndTime
      */
     public String getTenderSubmissionEndTime() {
          return tenderSubmissionEndTime;
     }



     /**
      * @param tenderSubmissionEndTime the tenderSubmissionEndTime to set
      */
     public void setTenderSubmissionEndTime(String tenderSubmissionEndTime) {
          this.tenderSubmissionEndTime = tenderSubmissionEndTime;
     }



     /**
      * @return the tenderOpeningDate
      */
     public String getTenderOpeningDate() {
          return tenderOpeningDate;
     }



     /**
      * @param tenderOpeningDate the tenderOpeningDate to set
      */
     public void setTenderOpeningDate(String tenderOpeningDate) {
          this.tenderOpeningDate = tenderOpeningDate;
     }



     /**
      * @return the tenderOpeningTime
      */
     public String getTenderOpeningTime() {
          return tenderOpeningTime;
     }



     /**
      * @param tenderOpeningTime the tenderOpeningTime to set
      */
     public void setTenderOpeningTime(String tenderOpeningTime) {
          this.tenderOpeningTime = tenderOpeningTime;
     }



     /**
      * @return the emd
      */
     public String getEmd() {
          return emd;
     }



     /**
      * @param emd the emd to set
      */
     public void setEmd(String emd) {
          this.emd = emd;
     }



     /**
      * @return the tenderFee
      */
     public String getTenderFee() {
          return tenderFee;
     }



     /**
      * @param tenderFee the tenderFee to set
      */
     public void setTenderFee(String tenderFee) {
          this.tenderFee = tenderFee;
     }



     /**
      * @return the estimatedTenderValue
      */
     public String getEstimatedTenderValue() {
          return estimatedTenderValue;
     }



     /**
      * @param estimatedTenderValue the estimatedTenderValue to set
      */
     public void setEstimatedTenderValue(String estimatedTenderValue) {
          this.estimatedTenderValue = estimatedTenderValue;
     }



     /**
      * @return the bg_dd_sc
      */
     public String getBg_dd_sc() {
          return bg_dd_sc;
     }



     /**
      * @param bg_dd_sc the bg_dd_sc to set
      */
     public void setBg_dd_sc(String bg_dd_sc) {
          this.bg_dd_sc = bg_dd_sc;
     }



     /**
      * @return the quantity
      */
     public String getQuantity() {
          return quantity;
     }



     /**
      * @param quantity the quantity to set
      */
     public void setQuantity(String quantity) {
          this.quantity = quantity;
     }



     /**
      * @return the projectDuration
      */
     public String getProjectDuration() {
          return projectDuration;
     }



     /**
      * @param projectDuration the projectDuration to set
      */
     public void setProjectDuration(String projectDuration) {
          this.projectDuration = projectDuration;
     }



     /**
      * @return the webSite
      */
     public String getWebSite() {
          return webSite;
     }



     /**
      * @param webSite the webSite to set
      */
     public void setWebSite(String webSite) {
          this.webSite = webSite;
     }



     /**
      * @return the scopOfWork
      */
     public String getScopOfWork() {
          return scopOfWork;
     }



     /**
      * @param scopOfWork the scopOfWork to set
      */
     public void setScopOfWork(String scopOfWork) {
          this.scopOfWork = scopOfWork;
     }



     /**
      * @return the contactPersonName
      */
     public String getContactPersonName() {
          return contactPersonName;
     }



     /**
      * @param contactPersonName the contactPersonName to set
      */
     public void setContactPersonName(String contactPersonName) {
          this.contactPersonName = contactPersonName;
     }



     /**
      * @return the email
      */
     public String getEmail() {
          return email;
     }



     /**
      * @param email the email to set
      */
     public void setEmail(String email) {
          this.email = email;
     }



     /**
      * @return the contactNo
      */
     public String getContactNo() {
          return contactNo;
     }



     /**
      * @param contactNo the contactNo to set
      */
     public void setContactNo(String contactNo) {
          this.contactNo = contactNo;
     }



     /**
      * @return the requirmentFromClient
      */
     public String getRequirmentFromClient() {
          return requirmentFromClient;
     }



     /**
      * @param requirmentFromClient the requirmentFromClient to set
      */
     public void setRequirmentFromClient(String requirmentFromClient) {
          this.requirmentFromClient = requirmentFromClient;
     }



     /**
      * @return the requirmentFromMascon
      */
     public String getRequirmentFromMascon() {
          return requirmentFromMascon;
     }



     /**
      * @param requirmentFromMascon the requirmentFromMascon to set
      */
     public void setRequirmentFromMascon(String requirmentFromMascon) {
          this.requirmentFromMascon = requirmentFromMascon;
     }



     /**
      * @return the isMasconEligibleToBid
      */
     public String getIsMasconEligibleToBid() {
          return isMasconEligibleToBid;
     }



     /**
      * @param isMasconEligibleToBid the isMasconEligibleToBid to set
      */
     public void setIsMasconEligibleToBid(String isMasconEligibleToBid) {
          this.isMasconEligibleToBid = isMasconEligibleToBid;
     }



     /* (non-Javadoc)
      * @see java.lang.Object#toString()
      */
     @Override
     public String toString() {
	  return "TenderDTO [tenderId=" + tenderId + ", tenderReferenceNo=" + tenderReferenceNo + ", organisationChain=" + organisationChain + ", locationDistrict="
	            + locationDistrict + ", state=" + state + ", projectDesccription=" + projectDesccription + ", preBidMeetingDate=" + preBidMeetingDate + ", preBidMeetingTime="
	            + preBidMeetingTime + ", tenderSubmissionEndDate=" + tenderSubmissionEndDate + ", tenderSubmissionEndTime=" + tenderSubmissionEndTime + ", tenderOpeningDate="
	            + tenderOpeningDate + ", tenderOpeningTime=" + tenderOpeningTime + ", emd=" + emd + ", tenderFee=" + tenderFee + ", estimatedTenderValue="
	            + estimatedTenderValue + ", bg_dd_sc=" + bg_dd_sc + ", quantity=" + quantity + ", projectDuration=" + projectDuration + ", webSite=" + webSite + ", scopOfWork="
	            + scopOfWork + ", contactPersonName=" + contactPersonName + ", email=" + email + ", contactNo=" + contactNo + ", requirmentFromClient=" + requirmentFromClient
	            + ", requirmentFromMascon=" + requirmentFromMascon + ", isMasconEligibleToBid=" + isMasconEligibleToBid + "]";
     }



     


   
}
