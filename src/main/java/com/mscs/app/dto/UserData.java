package com.mscs.app.dto;

/**
 * 
 * @author Himmat This Pojo is used to response associate data to client.
 *
 */
public class UserData
{

     public static final String	GET_LIST_FOR_AM	= "select u from User u where isApproveByAM = :isApproveByAM and userRole = :userRole";
     public static final String	GET_LIST_FOR_FM	= "select u from User u where isApproveByAM = :isApproveByAM and isApproveByFM = :isApproveByFM and userRole = :userRole";
     public static final String	GET_LIST_FOR_D	= "select u from User u where isApproveByAM = :isApproveByAM and isApproveByFM = :isApproveByFM and isApproveByD = :isApproveByD and userRole = :userRole";
     public static final String	GET_LIST_FOR_OP	= "select u from User u where isApproveByD =:isApproveByD and userRole = :userRole";

     private long		associateUserId;
     private String		associateName;
     private String		associateEmailId;
     private String		companyName;

     public UserData( long associateUserId , String associateName , String associateEmailId , String companyName ) {
	  super();
	  this.associateUserId = associateUserId;
	  this.associateName = associateName;
	  this.associateEmailId = associateEmailId;
	  this.companyName = companyName;
     }

     public long getAssociateUserId() {
	  return associateUserId;
     }

     public void setAssociateUserId(long associateUserId) {
	  this.associateUserId = associateUserId;
     }

     public String getAssociateName() {
	  return associateName;
     }

     public void setAssociateName(String associateName) {
	  this.associateName = associateName;
     }

     public String getAssociateEmailId() {
	  return associateEmailId;
     }

     public void setAssociateEmailId(String associateEmailId) {
	  this.associateEmailId = associateEmailId;
     }

     public String getCompanyName() {
	  return companyName;
     }

     public void setCompanyName(String companyName) {
	  this.companyName = companyName;
     }

}
