package com.mscs.app.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mscs.app.model.PasswordResetToken;
import com.mscs.app.model.User;

@Repository
public class PasswordTokenRepository
{

     @Autowired
     private SessionFactory sessionFactory;

     /**
      * used to get current hibernate session
      * 
      * @return
      */
     private Session getCurrentSession() {

	  return sessionFactory.getCurrentSession();

     }

     /**
      * used for save PasswordResetToken entity
      * 
      * @param passwordToken
      */
     public void save(PasswordResetToken passwordToken) {
	  getCurrentSession().save(passwordToken);
     }

     public PasswordResetToken findByToken(String passwordToken) {
	  // TODO Auto-generated method stub

	  List<PasswordResetToken> tokens = getCurrentSession().createQuery(PasswordResetToken.FIND_BY_TOKEN).setParameter("token", passwordToken).list();

	  return tokens.size() == 1 ? tokens.get(0) : null;

     }

     public PasswordResetToken getTokenByUser(final User user) {

	  PasswordResetToken tokens = (PasswordResetToken) getCurrentSession().createQuery(PasswordResetToken.FIND_TOKEN_BY_USER).setParameter("user", user);

	  return tokens;
     }

     public void mergeToekn(PasswordResetToken paResetToken) {
	  // TODO Auto-generated method stub

	  getCurrentSession().merge(paResetToken);

     }
}