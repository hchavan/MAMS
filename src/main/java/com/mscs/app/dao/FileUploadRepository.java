
package com.mscs.app.dao;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mscs.app.model.DocTypes;
import com.mscs.app.model.UploadedFile;

/**
 *
 * Repository class for the File entity
 *
 */
@Repository
@Transactional
public class FileUploadRepository
{

     @Autowired
     private SessionFactory sessionFactory;

     private Session getCurrentSession() {

	  return sessionFactory.getCurrentSession();

     }

     /**
      * finds all files from database
      *
      */
     public List<UploadedFile> findAllFiles() {

	  List<UploadedFile> files = getCurrentSession().createQuery(UploadedFile.FIND_ALLFILES).list();
	  return files;
     }

     /**
      * finds all files of given user
      *
      */
     public List<UploadedFile> findUsersFiles(String userName) {

	  List<UploadedFile> files = getCurrentSession().createQuery(UploadedFile.FIND_USERSFILES)
	            .setParameter("username", userName).list();
	  return files;
     }

     /**
      * finds a file given its name
      *
      * @param name
      *             - the file name of the searched file
      * @return a matching file name, or null if no file name found.
      */

     public UploadedFile findFileByFilename(String fileName, String size) {

	  List<UploadedFile> files = getCurrentSession().createQuery(UploadedFile.FIND_BY_FILENAMEANDSIZE)
	            .setParameter("name", fileName).setParameter("size", size).list();

	  return files.size() == 1 ? files.get(0) : null;
     }

     /**
      *
      * save changes made to a file, or insert it if its new
      *
      * @param file
      */
     public void save(UploadedFile file) {
	  getCurrentSession().merge(file);
     }

     /**
      * checks if a file name is still available in the database
      *
      * @param name
      *             - the file name to be checked for availability
      * @return true if the file name is still available
      */
     public boolean isFileNameAvailable(String fileName, Long size) {

	  List<UploadedFile> files = getCurrentSession().createQuery(UploadedFile.FIND_BY_FILENAMEANDSIZE)
	            .setParameter("name", fileName).setParameter("size", size).list();

	  return files.isEmpty();
     }

     public boolean isFileNameSizePathAvailable(String fileName, Long size, String path) {

	  List<UploadedFile> files = getCurrentSession().createQuery(UploadedFile.FIND_BY_FILENAMEANDSIZE_AND_PATH)
	            .setParameter("name", fileName).setParameter("size", size).setParameter("path", path).list();

	  return files.isEmpty();
     }

  

     public int deleteFile(Long id) {
	  // TODO Auto-generated method stub

	  /*
	   * int
	   * i=em.createNamedQuery(UploadedFile.DELETE_FILE_BY_ID,UploadedFile.
	   * class) .setParameter("id", id) .executeUpdate();
	   * 
	   * return i;
	   */

	  Query query = (Query) getCurrentSession().createQuery("DELETE FROM UploadedFile AS f WHERE  f.id=:id");

	  query.setParameter("id", id);
	  int result = query.executeUpdate();

	  return result;

     }

     public List<UploadedFile> findFilesByInstName(String instituteName) {
	  // TODO Auto-generated method stub
	  List<UploadedFile> files = getCurrentSession().createQuery(UploadedFile.FIND_FILES_BY_INSTNAME)
	            .setParameter("instituteName", instituteName).list();

	  return files;
     }

     public List<UploadedFile> getFilesOfUser(String userName) {
	  // TODO Auto-generated method stub
	  List<UploadedFile> rowDocs = getCurrentSession()
	            .createQuery(UploadedFile.FIND_FILES_BY_USERNAME)
	            .setParameter("username", userName).list();
	  return rowDocs;
     }

     public UploadedFile downloadRowDocFile(Long id) {
	  return (UploadedFile) getCurrentSession().get(UploadedFile.class, id);
     }

     public DocTypes getDocTypeById(long parseInt) {
	  // TODO Auto-generated method stub

	  DocTypes docTypes = (DocTypes) getCurrentSession().get(DocTypes.class, parseInt);

	  return docTypes;
     }

}