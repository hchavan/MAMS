package com.mscs.app.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mscs.app.dto.UserData;
import com.mscs.app.model.User;

/**
 *
 * Repository class for the User entity
 *
 */
@Repository
public class UserRepository
{

     @Autowired
     private SessionFactory sessionFactory;

     /**
      * used to get current hibernate session
      * 
      * @return
      */
     private Session getCurrentSession() {

	  return sessionFactory.getCurrentSession();

     }

     /**
      * used for save user entity
      * 
      * @param user
      */
     public void save(User user) {
	  getCurrentSession().save(user);
     }

     /**
      * used for merge user entity
      * 
      * @param user
      */
     public void mergeUser(User user) {
	  // TODO Auto-generated method stub
	  getCurrentSession().merge(user);
     }

     /**
      * finds a user given its username
      *
      * @param username
      *             - the username of the searched user
      * @return a matching user, or null if no user found.
      */
     @Transactional()
     public User findUserByUsername(String username) {

	  System.out.println("============PPPPPPPPPPPPPPPPPPPP");
	  @SuppressWarnings("unchecked")
	  List<User> users = getCurrentSession().createQuery(User.FIND_BY_USERNAME).setParameter("username", username).list();

	  return users.size() == 1 ? users.get(0) : null;
     }

     /**
      * checks if a username is still available in the database
      *
      * @param username
      *             - the username to be checked for availability
      * @return true if the username is still available
      */
     public boolean isUsernameAvailable(String username) {

	  List<User> users = getCurrentSession().createQuery(User.FIND_BY_USERNAME).setParameter("username", username).list();

	  return users.isEmpty();
     }

     public boolean checkUserisAvailableOrNot(String username) {

	  List<User> users = getCurrentSession().createQuery(User.FIND_BY_USERNAME).setParameter("username", username).list();

	  return users.size() == 1 ? true : false;
     }

     /**
      * get the list of associate user for Account manager which are not approve
      * by him.
      * 
      * @return
      */
     public List<UserData> getAssociateListForAM() {
	  // TODO Auto-generated method stub

	  List<User> userList = getCurrentSession().createQuery(UserData.GET_LIST_FOR_AM).setParameter("isApproveByAM", false).setParameter("userRole", "ASSOCIATE").list();

	  List<UserData> associateListForAM = new ArrayList<>();

	  for (User user : userList) {
	       UserData associateData = new UserData(user.getId(), user.getAssociateName(), user.getAssociateEmail(), user.getCompanyName());

	       associateListForAM.add(associateData);
	  }

	  return associateListForAM;
     }

     /**
      * get the single associate user with all detail
      * 
      * @param associateUserId
      * @return
      */
     public User getAssociateUser(long associateUserId) {
	  // TODO Auto-generated method stub

	  User user = (User) getCurrentSession().get(User.class, associateUserId);

	  return user;
     }

     /**
      * get the list of associate user for Finance manager which are approved by
      * Acount manager
      * 
      * @return
      */
     public List<UserData> getAssociateListForFM() {
	  // TODO Auto-generated method stub
	  List<User> userList = getCurrentSession().createQuery(UserData.GET_LIST_FOR_FM).setParameter("isApproveByAM", true).setParameter("isApproveByFM", false)
	            .setParameter("userRole", "ASSOCIATE").list();

	  List<UserData> associateListForAM = new ArrayList<>();

	  for (User user : userList) {
	       UserData associateData = new UserData(user.getId(), user.getAssociateName(), user.getAssociateEmail(), user.getCompanyName());

	       associateListForAM.add(associateData);
	  }

	  return associateListForAM;
     }

     /**
      * get List of associate user approved by AM and FM
      * 
      * @return
      */
     public List<UserData> getAssociateListForD() {
	  // TODO Auto-generated method stub
	  List<User> userList = getCurrentSession().createQuery(UserData.GET_LIST_FOR_D).setParameter("isApproveByAM", true).setParameter("isApproveByFM", true)
	            .setParameter("isApproveByD", false).setParameter("userRole", "ASSOCIATE").list();

	  List<UserData> associateListForAM = new ArrayList<>();

	  for (User user : userList) {
	       UserData associateData = new UserData(user.getId(), user.getAssociateName(), user.getAssociateEmail(), user.getCompanyName());

	       associateListForAM.add(associateData);
	  }

	  return associateListForAM;
     }

     /**
      * get list of associate user approved by all(FM,AM,Director)for OP
      * manager.
      * 
      * @return
      */
     public List<UserData> getAssociateListForOP() {
	  // TODO Auto-generated method stub
	  List<User> userList = getCurrentSession().createQuery(UserData.GET_LIST_FOR_OP).setParameter("isApproveByD", true).setParameter("userRole", "ASSOCIATE").list();

	  List<UserData> associateListForOP = new ArrayList<>();

	  for (User user : userList) {
	       UserData associateData = new UserData(user.getId(), user.getAssociateName(), user.getAssociateEmail(), user.getCompanyName());

	       associateListForOP.add(associateData);
	  }

	  return associateListForOP;
     }

}
