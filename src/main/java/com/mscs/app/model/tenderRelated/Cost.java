
package com.mscs.app.model.tenderRelated;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.mscs.app.model.AbstractEntity;

@Entity
public class Cost extends AbstractEntity
{

     private String	   cosMilestone;
     private String	   cosDateP;
     private String	   cosAmountP;
     private String	   cosDateA;
     private String	   cosAmountA;
     @ManyToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "tenderProject_id")
     private TenderProject tenderProject_id;

     public Cost() {

     }

     public Cost( String cosMilestone , String cosDateP , String cosAmountP , String cosDateA , String cosAmountA , TenderProject tenderProject_id ) {
	  super();
	  this.cosMilestone = cosMilestone;
	  this.cosDateP = cosDateP;
	  this.cosAmountP = cosAmountP;
	  this.cosDateA = cosDateA;
	  this.cosAmountA = cosAmountA;
	  this.tenderProject_id = tenderProject_id;
     }

     /**
      * @return the cosMilestone
      */
     public String getCosMilestone() {
          return cosMilestone;
     }

     /**
      * @param cosMilestone the cosMilestone to set
      */
     public void setCosMilestone(String cosMilestone) {
          this.cosMilestone = cosMilestone;
     }

     /**
      * @return the cosDateP
      */
     public String getCosDateP() {
          return cosDateP;
     }

     /**
      * @param cosDateP the cosDateP to set
      */
     public void setCosDateP(String cosDateP) {
          this.cosDateP = cosDateP;
     }

     /**
      * @return the cosAmountP
      */
     public String getCosAmountP() {
          return cosAmountP;
     }

     /**
      * @param cosAmountP the cosAmountP to set
      */
     public void setCosAmountP(String cosAmountP) {
          this.cosAmountP = cosAmountP;
     }

     /**
      * @return the cosDateA
      */
     public String getCosDateA() {
          return cosDateA;
     }

     /**
      * @param cosDateA the cosDateA to set
      */
     public void setCosDateA(String cosDateA) {
          this.cosDateA = cosDateA;
     }

     /**
      * @return the cosAmountA
      */
     public String getCosAmountA() {
          return cosAmountA;
     }

     /**
      * @param cosAmountA the cosAmountA to set
      */
     public void setCosAmountA(String cosAmountA) {
          this.cosAmountA = cosAmountA;
     }

     /**
      * @return the tenderProject_id
      */
     public TenderProject getTenderProject_id() {
          return tenderProject_id;
     }

     /**
      * @param tenderProject_id the tenderProject_id to set
      */
     public void setTenderProject_id(TenderProject tenderProject_id) {
          this.tenderProject_id = tenderProject_id;
     }

     /* (non-Javadoc)
      * @see java.lang.Object#toString()
      */
     @Override
     public String toString() {
	  return "Cost [cosMilestone=" + cosMilestone + ", cosDateP=" + cosDateP + ", cosAmountP=" + cosAmountP + ", cosDateA=" + cosDateA + ", cosAmountA=" + cosAmountA
	            + ", tenderProject_id=" + tenderProject_id + "]";
     }

     
}
