package com.mscs.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * The File JPA entity.
 *
 */
@Entity
@Table(name = "uploaded_file")
public class UploadedFile extends AbstractEntity
{

     public static final String	FIND_BY_FILENAMEANDSIZE			 = "select f from UploadedFile f where name = :name and size = :size";
     public static final String	FIND_BY_FILENAMEANDSIZE_AND_PATH	 = "select f from UploadedFile f where name = :name and size = :size and path = :path";
     public static final String	FIND_USERSFILES				 = "select f from UploadedFile f where owner = :username";
     public static final String	FIND_FILE_BY_ID				 = "select f from UploadedFile f where id = :id";
     public static final String	DELETE_FILE_BY_ID			 = "delete from UploadedFile  where id = :id";
     public static final String	FIND_ALLFILES				 = "select f from UploadedFile f";
     public static final String	FIND_FILES_BY_INSTNAME			 = "select f from UploadedFile f where f.instituteName = :instituteName";
     public static final String	FIND_FILES_BY_USERNAME = "select f from UploadedFile f where owner = :username";

     private String		name;
     private String		type;
     private long		size;
     private String		creationDate;
     private String		modificationDate;
     private String		owner;
     private String		path;
     @OneToOne(targetEntity = DocTypes.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
     @JoinColumn(nullable = false, name = "docTypeId")
     private DocTypes		docType;

     public UploadedFile() {

     }

     public UploadedFile( String name , String type , long size , String creationDate , String modificationDate ,
               String owner , String path , DocTypes docType ) {
	  super();
	  this.name = name;
	  this.type = type;
	  this.size = size;
	  this.creationDate = creationDate;
	  this.modificationDate = modificationDate;
	  this.owner = owner;
	  this.path = path;
	  this.docType = docType;
     }

     public String getName() {
	  return name;
     }

     public void setName(String name) {
	  this.name = name;
     }

     public String getType() {
	  return type;
     }

     public void setType(String type) {
	  this.type = type;
     }

     public long getSize() {
	  return size;
     }

     public void setSize(long size) {
	  this.size = size;
     }

     public String getCreationDate() {
	  return creationDate;
     }

     public void setCreationDate(String creationDate) {
	  this.creationDate = creationDate;
     }

     public String getModificationDate() {
	  return modificationDate;
     }

     public void setModificationDate(String modificationDate) {
	  this.modificationDate = modificationDate;
     }

     public String getOwner() {
	  return owner;
     }

     public void setOwner(String owner) {
	  this.owner = owner;
     }

     public String getPath() {
	  return path;
     }

     public void setPath(String path) {
	  this.path = path;
     }

     public DocTypes getDocType() {
	  return docType;
     }

     public void setDocType(DocTypes docType) {
	  this.docType = docType;
     }

     @Override
     public String toString() {
	  return "UploadedFile [name=" + name + ", type=" + type + ", size=" + size + ", creationDate=" + creationDate
	            + ", modificationDate=" + modificationDate + ", owner=" + owner + ", path=" + path + ", docType="
	            + docType + "]";
     }

}