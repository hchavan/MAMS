package com.mscs.app.model;

import javax.persistence.Entity;

@Entity
public class DocTypes extends AbstractEntity
{

     private String doctype;

     public DocTypes() {
	  super();
     }

     public DocTypes( String doctype ) {
	  super();
	  this.doctype = doctype;
     }

     public String getDoctype() {
	  return doctype;
     }

     public void setDoctype(String doctype) {
	  this.doctype = doctype;
     }

     @Override
     public String toString() {
	  return "DocTypes [doctype=" + doctype + ", getId()=" + getId() + "]";
     }

}
