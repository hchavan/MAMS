package com.mscs.configuration.springSecurity;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
/**
 * 
 * @author Himmat
 * 
 * This class register spring security config file to war
 *
 */

public class SecurityWebAppInitializer extends AbstractSecurityWebApplicationInitializer{

}
