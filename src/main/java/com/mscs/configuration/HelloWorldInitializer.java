package com.mscs.configuration;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.mscs.configuration.springSecurity.AppSecurityConfiguration;

public class HelloWorldInitializer extends AbstractAnnotationConfigDispatcherServletInitializer
{

     @Override
     protected Class<?>[] getRootConfigClasses() {
	  return new Class<?>[] { RootContextConfig.class, DevelopmentConfiguration.class,
	            AppSecurityConfiguration.class };
     }

     @Override
     protected Class<?>[] getServletConfigClasses() {
	  return new Class<?>[] { WebConfiguration.class };
     }

     @Override
     protected String[] getServletMappings() {
	  return new String[] { "/" };
     }

     /*
      * @Override protected Filter[] getServletFilters() { return new
      * Filter[]{new HiddenHttpMethodFilter(), new MultipartFilter(), new
      * OpenEntityManagerInViewFilter()}; }
      */

     @Override
     protected Filter[] getServletFilters() {

	  CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
	  characterEncodingFilter.setEncoding("UTF-8");
	  HiddenHttpMethodFilter hiddenHttpMethodFilter = new HiddenHttpMethodFilter();

	  return new Filter[] { characterEncodingFilter };
     }

     /*
      * @Override public void onStartup(ServletContext servletContext) throws
      * ServletException { // TODO Auto-generated method stub
      * super.onStartup(servletContext); servletContext.addListener(new
      * AppHttpSessionListener()); //
      * servletContext.addListener(AppHttpSessionListener.class); }
      */

     @Override
     protected void customizeRegistration(ServletRegistration.Dynamic registration) {
	  registration.setMultipartConfig(getMultipartConfigElement());
     }

     private MultipartConfigElement getMultipartConfigElement() {
	  MultipartConfigElement multipartConfigElement = new MultipartConfigElement(LOCATION, MAX_FILE_SIZE,
	            MAX_REQUEST_SIZE, FILE_SIZE_THRESHOLD);
	  return multipartConfigElement;
     }

     /* Set these variables for your project needs */

     // private static final String LOCATION = "home/docs/uploaded-files/";

     private static final String LOCATION	     = "C:\\mytemp\\";

     private static final long	 MAX_FILE_SIZE	     = 1024 * 1024 * 10;// 10MB

     private static final long	 MAX_REQUEST_SIZE    = 1024 * 1024 * 30;// 30MB

     private static final int	 FILE_SIZE_THRESHOLD = 0;

}