package com.mscs.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * Development specific configuration - creates a localhost postgresql
 * datasource, sets hibernate on create drop mode and inserts some test data on
 * the database.
 *
 * Set -Dspring.profiles.active=development to activate this config.
 *
 */
@Configuration
@EnableTransactionManagement
public class DevelopmentConfiguration
{

     @Autowired
     private Environment env;

     @Bean

     public DataSource dataSource() {

	  DriverManagerDataSource dataSource = new DriverManagerDataSource();

	  dataSource.setDriverClassName("com.mysql.jdbc.Driver");

	  
/*
	  dataSource.setUrl("jdbc:mysql://192.168.0.33:3306/mams?createDatabaseIfNotExist=true");

	  dataSource.setUsername("rj");

	  dataSource.setPassword("rj0078");
*/
	 
	  dataSource.setUrl("jdbc:mysql://localhost:3306/mams?createDatabaseIfNotExist=true");
		
	  dataSource.setUsername("root");
		
	  dataSource.setPassword("ca$hc0w");
	 
		   
	  return dataSource;

     }

     @Bean

     public LocalSessionFactoryBean sessionFactory() {

	  LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();

	  sessionFactoryBean.setDataSource(dataSource());

	  sessionFactoryBean.setPackagesToScan(new String[] { "com.mscs.app.model" });

	  sessionFactoryBean.setHibernateProperties(hibProperties());

	  return sessionFactoryBean;

     }

     private Properties hibProperties() {

	  Properties properties = new Properties();

	  properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
	  properties.put("hibernate.hbm2ddl.auto", "update");
	  properties.put("hibernate.show_sql", "true");
	  properties.put("hibernate.format_sql", "true");
	  properties.put("hibernate.use_sql_comments", "true");
	  return properties;

     }

     @Bean

     public HibernateTransactionManager transactionManager() {

	  HibernateTransactionManager transactionManager = new HibernateTransactionManager();

	  transactionManager.setSessionFactory(sessionFactory().getObject());

	  return transactionManager;

     }

     /*
      * @Bean
      * 
      * public UrlBasedViewResolver setupViewResolver() {
      * 
      * UrlBasedViewResolver resolver = new UrlBasedViewResolver();
      * 
      * resolver.setPrefix("/WEB-INF/pages/");
      * 
      * resolver.setSuffix(".jsp");
      * 
      * resolver.setViewClass(JstlView.class);
      * 
      * return resolver;
      * 
      * }
      */
     /*
      * @Bean(name = "datasource") public DriverManagerDataSource dataSource() {
      * DriverManagerDataSource dataSource = new DriverManagerDataSource();
      * dataSource.setDriverClassName("com.mysql.jdbc.Driver");
      * dataSource.setUrl(
      * "jdbc:mysql://localhost:3306/rdsdbtest?createDatabaseIfNotExist=true");
      * dataSource.setUsername("root"); dataSource.setPassword(""); return
      * dataSource; }
      * 
      * @Bean(name = "entityManagerFactory") public
      * LocalContainerEntityManagerFactoryBean
      * entityManagerFactory(DriverManagerDataSource dataSource) {
      * 
      * LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new
      * LocalContainerEntityManagerFactoryBean();
      * entityManagerFactoryBean.setDataSource(dataSource);
      * entityManagerFactoryBean.setPackagesToScan(new
      * String[]{"com.aibsinc.rds.osal.app.model"});
      * entityManagerFactoryBean.setLoadTimeWeaver(new
      * InstrumentationLoadTimeWeaver());
      * entityManagerFactoryBean.setJpaVendorAdapter(new
      * HibernateJpaVendorAdapter());
      * 
      * Map<String, Object> jpaProperties = new HashMap<String, Object>();
      * jpaProperties.put("hibernate.hbm2ddl.auto", "update");
      * jpaProperties.put("hibernate.show_sql", "true");
      * jpaProperties.put("hibernate.format_sql", "true");
      * jpaProperties.put("hibernate.use_sql_comments", "true");
      * jpaProperties.put("hibernate.dialect",
      * "org.hibernate.dialect.MySQLDialect");
      * entityManagerFactoryBean.setJpaPropertyMap(jpaProperties);
      * 
      * return entityManagerFactoryBean; }
      */

}
