package com.mscs.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 *
 * The root context configuration of the application - the beans in this context
 * will be globally visible in all servlet contexts.
 *
 */

@Configuration
@ComponentScan({ "com.mscs.app.services", "com.mscs.app.dao" })
@PropertySource("classpath:email.properties")
public class RootContextConfig
{

     @Autowired
     private Environment env;

     /*
      * @Bean(name = "transactionManager") public PlatformTransactionManager
      * transactionManager(EntityManagerFactory entityManagerFactory,
      * DriverManagerDataSource dataSource) { JpaTransactionManager
      * transactionManager = new JpaTransactionManager();
      * transactionManager.setEntityManagerFactory(entityManagerFactory);
      * transactionManager.setDataSource(dataSource); return transactionManager;
      * }
      */

     // beans

     /*
      * @Bean public Properties Properties() { Properties jMailProps = new
      * Properties(); jMailProps.put("mail.smtp.auth", true);
      * jMailProps.put("mail.smtp.starttls.enable",true);
      * jMailProps.put("mail.smtp.host", env.getProperty("smtp.host"));
      * jMailProps.put("mail.smtp.port", env.getProperty("smtp.port",
      * Integer.class)); return jMailProps;
      * 
      * }
      */

     @Bean
     public JavaMailSenderImpl javaMailSenderImpl() {
	  final JavaMailSenderImpl mailSenderImpl = new JavaMailSenderImpl();
	  mailSenderImpl.setHost(env.getProperty("smtp.host"));
	  mailSenderImpl.setPort(env.getProperty("smtp.port", Integer.class));
	  mailSenderImpl.setProtocol(env.getProperty("smtp.protocol"));
	  mailSenderImpl.setUsername(env.getProperty("smtp.username"));
	  mailSenderImpl.setPassword(env.getProperty("smtp.password"));
	  final Properties javaMailProps = new Properties();
	  javaMailProps.put("mail.smtp.auth", true);
	  javaMailProps.put("mail.smtp.starttls.enable", true);
	  mailSenderImpl.setJavaMailProperties(javaMailProps);
	  return mailSenderImpl;
     }

     @Bean
     public ThreadPoolTaskExecutor taskExecutor() {
	  ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
	  pool.setCorePoolSize(5);
	  pool.setMaxPoolSize(10);
	  pool.setWaitForTasksToCompleteOnShutdown(true);
	  return pool;
     }
}
